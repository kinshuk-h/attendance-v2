<?php

require_once("database.php");
require_once('vendor\autoload.php');

function getAccessToken() {
    $client = new Google_Client();
    $client->setAuthConfig("attendance-143b5-firebase-adminsdk-u4f43-d673480b33.json");
    $client->addScope("https://www.googleapis.com/auth/firebase.messaging");
    $client->fetchAccessTokenWithAssertion();
    return $client->getAccessToken()['access_token'];
}
function sendMessage($payload) {
    $url = "https://fcm.googleapis.com/v1/projects/attendance-143b5/messages:send";
    $headers = array("Content-Type: application/json", 
                        "Authorization: Bearer ".getAccessToken());
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(array("message"=>$payload)));
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $res = curl_exec($curl); curl_close($curl);
}

try {
    date_default_timezone_set("Asia/Kolkata"); $db = new Database();
    if($_SERVER['REQUEST_METHOD']=='GET') {
        $auth = getAuthorization();
        if($auth!=null) {
            $user_id = $auth['user_id']; $access_token = $auth['access_token'];
            if(isEmail($user_id)) {
                $user = $db->findATeacher($user_id);
                if($user!=null && $user->accessToken==$access_token) {
                    if(allSet($_GET, 'PID', 'SEC_GROUP', 'TYPE')) {
                        $pid = $_GET['PID']; $group = $_GET['SEC_GROUP']; $type = $_GET['TYPE'];
                        $requests = $db->getRequestsForPaper($pid, $group, $type);
                        print_r(json_encode($requests)); http_response_code(200);
                    } else http_response_code(400);
                } else http_response_code(401);
            } else {
                $user = $db->findAStudent($user_id, true);
                if($user!=null && $user->accessToken==$access_token) {
                    $requests = $db->getRequestsByCreator($user->rollNo);
                    print_r(json_encode($requests));
                    http_response_code(200);
                } else http_response_code(401);
            }
        } else http_response_code(400);
    } else if($_SERVER['REQUEST_METHOD']=='POST') {
        $auth = getAuthorization();
        if($auth!=null && allSet($_POST,'CREATOR','ROLL_NO','PID','SEC_GROUP','PAPER','TYPE')) {
            $user_id = $auth['user_id']; $access_token = $auth['access_token'];
            if(isEmail($user_id)) {
                $user = $db->findATeacher($user_id);
                if($user!=null && $user->accessToken==$access_token) {
                    $request = Request::from($_POST); $db->updateRequest($request);
                    $payload = ["token"=>$db->getStudentDeviceToken($_POST['CREATOR']),
                                "data"=>$request->toArray()];
                    if($db->getStudentNotificationStatus($_POST['CREATOR'])===1) {
                        $payload += ["notification"=>
                            ["title"=>"Request update for {$request->rollNo}",
                             "body" =>"The request was ".
                                      ($request->appproved?0:1?"approved":"declined").
                                      " by the teacher"]];
                    }
                    sendMessage($payload); http_response_code(200);
                } else http_response_code(401);
            } else {
                $user = $db->findAStudent($user_id, true);
                if($user!=null && $user->accessToken==$access_token) {
                    $request = Request::from($_POST);
                    $request->name = $db->getName($request->rollNo);
                    if(!$db->addRequest($request)) { http_response_code(400); return; }
                    $token = $db->getTeacherDeviceToken($request->paperId, 
                                                        $request->group, $request->type);
                    $payload = [ "token"=>$token, "data"=>$request->toArray() ];
                    $email = $db->getTeacherId($request->paperId, 
                                                $request->group, $request->type);
                    if($db->getTeacherNotificationStatus($email)===1) {
                        $payload += ["notification"=>
                            ["title"=>"New request for {$request->rollNo}",
                             "body"=>"Request originated from {$request->creator}"]];
                    }
                    sendMessage($payload); http_response_code(200);
                } else http_response_code(401);
            }
        } else if($auth!=null && allSet($_POST, 'PID', 'SEC_GROUP', 'TYPE')) {
            $user_id = $auth['user_id']; $access_token = $auth['access_token'];
            if(isEmail($user_id)) {
                $user = $db->findATeacher($user_id);
                if($user!=null && $user->accessToken==$access_token) {
                    $pid = $_POST['PID']; $group = $_POST['SEC_GROUP']; 
                    $paper = $_POST['PAPER'] ?? null; $type = $_POST['TYPE'];
                    $db->generateRequests($pid, $group, $type, $paper);
                    http_response_code(200);
                }
            }
        } else if($auth!=null) {
            $user_id = $auth['user_id']; $access_token = $auth['access_token'];
            if(isEmail($user_id)) {
                $user = $db->findATeacher($user_id);
                if($user!=null && $user->accessToken==$access_token) {
                    $data = json_decode(file_get_contents("php://input"), true);
                    if($data != null) {
                        foreach($data as $array) {
                            $request = Request::from($array); $db->updateRequest($request);
                            $payload = ["token"=>$db->getStudentDeviceToken($request->creator),
                                        "data"=>$request->toArray()];
                            if($db->getStudentNotificationStatus($request->creator)===1) {
                                $payload += ["notification"=>
                                    ["title"=>"Request update for {$request->rollNo}",
                                     "body" =>"The request was ".
                                              ($request->appproved?0:1?"approved":"declined").
                                              " by the teacher"]];
                            }
                            sendMessage($payload);
                        } http_response_code(200);
                    } else http_response_code(400);
                } else http_response_code(401);
            } else http_response_code(400);
        } else http_response_code(400);
    } else http_response_code(400);
}
catch(Exception $e) { http_response_code(418); print_r($e->getMessage()); }

?>