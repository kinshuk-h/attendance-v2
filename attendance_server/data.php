<?php

function isEmail($email) {
    return preg_match_all("/[a-z0-9._]+@([a-z][a-z0-9]*)([.][a-z][a-z0-9]+)+/", $email);
}
function endsWith($string, $substring) {
    return substr_compare($string, $substring, -strlen($substring)) === 0;
}
function allSet($array, ...$keys) {
    foreach($keys as $key) { if(!isset($array[$key])) return false; }
    return true;
}
function getAuthorization($type = "Basic") {
    if(function_exists("getallheaders") && isset(getallheaders()['Authorization'])) {
        $authentication = getallheaders()['Authorization'];
        $auth_params = explode(' ', $authentication);
        if($auth_params[0]==$type) {
            $hash = array_pop($auth_params);
            $hash = base64_decode($hash); $args = explode(':',$hash);
            return array("user_id"=>trim($args[0]), "access_token"=>trim($args[1]));
        } else return null;
    } return null;
}

class User {
    public $name, $rollNo, $password, $accessToken, $courseId, $email, $papers = [], $group;
    public function __construct($name) { $this->name = $name; }
    public function genUUID() { $this->accessToken = uniqid('user_',true); }
    public static function load($array) {
        if(!isset($array['NAME'])) return null;
        $user = new User($array['NAME']);
        $user->rollNo = $array['ROLL_NO'] ?? null;
        $user->email = $array['EMAIL'] ?? null;
        $user->courseId = $array['CID'] ?? null;
        $user->accessToken = $array['ACCESS_TOKEN'] ?? null;
        $user->password = $array['PASSWORD'] ?? null;
        $user->group = $array['SEC_GROUP'] ?? null;
        $user->parsePapers($array['PAPERS'] ?? null);
        return $user;
    }
    public function parsePapers($array) {
        if($array == null) return;
        foreach($array as $paper) {
            $new_paper = ["SEC_GROUP"=> $paper['SEC_GROUP'] ?? null, 
                          "PAPER"    => $paper['NAME'] ?? $paper['PAPER'] ?? null, 
                          "PID"      => $paper['PID'] ?? null,
                          "TYPE"     => $paper['TYPE'] ?? null];
            array_push($this->papers, $new_paper);
        }
    }
}

class Request {
    public $name = "", $creator, $rollNo, $approved = 0, $handled = 0, 
           $paperId, $paper, $date, $group, $type;
    public function __construct() {}
    public static function from($array) {
        $request = new Request();
        $request->name = $array['NAME'] ?? null;
        $request->creator = $array['CREATOR'];
        $request->rollNo = $array['ROLL_NO'];
        $request->paperId = $array['PID'];
        $request->group = $array['SEC_GROUP'];
        $request->type = $array['TYPE'] ?? null;
        $request->paper = $array['PAPER'] ?? null;
        $request->approved = $array['APPROVED'] ?? 0;
        $request->handled = $array['HANDLED'] ?? 0;
        if(isset($array['DATE'])) 
            $request->date = date_create_from_format("Y-m-d", $array['DATE']);
        return $request;
    }
    public function toArray() {
        return array("CREATOR"=>$this->creator, 
                     "ROLL_NO"=>$this->rollNo,
                     "PAPER"=>$this->paper,
                     "PID"=>$this->paperId,
                     "APPROVED"=>strval($this->approved),
                     "HANDLED"=>strval($this->handled),
                     "NAME"=>$this->name,
                     "SEC_GROUP"=>$this->group,
                     "TYPE"=>$this->type);
    }
}

?>