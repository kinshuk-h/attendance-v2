<?php

require_once("data.php");

define('ADMIN_KEY', 'r1m2r3f4a5b6y7s8s1i2r3k4');

class Database {
    private $server_name = "localhost";
    private $username = "root";
    private $password = "qwertyuiop@A1";
    private $database = "id12516350_college";
    private $conn;
    public function __construct() { 
        $this->conn = new PDO("mysql:host={$this->server_name};dbname={$this->database}", $this->username, $this->password);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    public function __destruct() { $this->conn = null; }
    public function getDatabase() { return $this->conn; }
    
    public function getName($roll_no) {
        $local = $this->conn;
        if($local!=null) {
            $stmt = $local->prepare("SELECT NAME FROM students WHERE roll_no={$roll_no}");
            $stmt->execute(); $result = $stmt->fetchAll();
            if(count($result)>0) return $result[0][0];
            else return null;
        }
    }
    public function findAStudent($user_id, $faster = false) {
        $local = $this->conn;
        if($local!=null) {
            $stmt = $local->prepare("SELECT * FROM students WHERE roll_no={$user_id}");
            $stmt->execute(); $result = $stmt->fetchAll(); if(count($result)==0) return null; 
            $user = User::load($result[0]); if($user == null) return null; if($faster) return $user;
            $stmt = $local->prepare("SELECT NAME, PID, SEC_GROUP, TYPE FROM papers WHERE 
                                     CID = {$user->courseId} AND SEC_GROUP='{$user->group}'
                                     ORDER BY TYPE DESC, PID ASC");
            $stmt->execute(); $user->parsePapers($stmt->fetchAll()); return $user;
        } else throw new Exception("Unable to connect to database");
    }
    public function findStudent($user_id, $MAC) {
        $local = $this->conn;
        if($local!=null) {
            $token = uniqid('user_',true);
            $local->exec("UPDATE students SET access_token='{$token}' WHERE roll_no={$user_id}");
            $stmt = $local->prepare("SELECT * FROM students WHERE roll_no={$user_id}");
            $stmt->execute(); $result = $stmt->fetchAll();
            if(count($result)==0) return null; $sMAC = $result[0]['LOGIN_MAC'];
            $stmt = $local->prepare("SELECT * FROM devices WHERE LOGIN_MAC='{$sMAC}'");
            $stmt->execute(); $device = $stmt->fetchAll(); $device=(count($device)!=0)?$device[0]:null;
            $user = $sMAC==null || $device==null || ($device!=null && $device['ONLINE']==0) 
                    ? User::load($result[0]) : null;
            if($user == null) return null;
            $stmt = $local->prepare("SELECT NAME, PID, SEC_GROUP, TYPE FROM papers WHERE 
                                     CID = {$user->courseId} AND SEC_GROUP='{$user->group}'
                                     ORDER BY TYPE DESC, PID ASC");
            $stmt->execute(); $user->parsePapers($stmt->fetchAll()); return $user;
        } else throw new Exception("Unable to connect to database");
    }
    public function getStudentDeviceToken($roll_no) {
        $local = $this->conn;
        if($local!=null) {
            $stmt = $local->prepare("SELECT LOGIN_MAC FROM students WHERE roll_no={$roll_no}");
            $stmt->execute(); $result = $stmt->fetchAll();
            if(count($result)==0 || $result[0]["LOGIN_MAC"]==null) return null;
            return $this->getDeviceToken($result[0]["LOGIN_MAC"]);
        } else throw new Exception("Unable to connect to database");
    }
    public function bindStudentWithDevice($user_id, $MAC) {
        $local = $this->conn;
        if($local!=null) {
            $local->exec("UPDATE students SET LOGIN_MAC='{$MAC}' WHERE roll_no={$user_id}");
            $stmt = $local->prepare("SELECT * FROM devices WHERE LOGIN_MAC='{$MAC}'");
            $stmt->execute(); $count = count($stmt->fetchAll());
            if($count == 0) { $local->exec("INSERT INTO devices(LOGIN_MAC) values ('{$MAC}')"); }
        } else throw new Exception("Unable to connect to database");
    }
    public function loginStudent($user_id) {
        $local = $this->conn;
        if($local!=null) {
            $stmt = $local->prepare("SELECT LOGIN_MAC from students WHERE roll_no={$user_id}");
            $stmt->execute(); $result = $stmt->fetchAll();
            if(count($result)==1) {
                $MAC = $result[0]['LOGIN_MAC'];
                if($MAC!=null) { $local->exec("UPDATE devices SET online=1 WHERE LOGIN_MAC='{$MAC}'"); return true; }
            }
        } else throw new Exception("Unable to connect to database");
        return false;
    }
    public function logoutStudent($user_id, $clear = false) {
        $local = $this->conn;
        if($local!=null) {
            $stmt = $local->prepare("SELECT LOGIN_MAC from students WHERE roll_no={$user_id}");
            $stmt->execute(); $result = $stmt->fetchAll();
            if(count($result)==1) {
                $MAC = $result[0]['LOGIN_MAC'];
                $local->exec("UPDATE devices SET online=0".($clear?",device_token=NULL":"")." WHERE LOGIN_MAC='{$MAC}'");
                if($clear) { $local->exec("UPDATE students SET LOGIN_MAC=NULL WHERE roll_no={$user_id}"); }
            }
        } else throw new Exception("Unable to connect to database");
    }
    public function updateStudentNotificationStatus($user_id, $notif) {
        $local = $this->conn;
        if($local != null) {
            $local->exec("UPDATE students SET notifications={$notif} WHERE roll_no={$user_id}");
        } else throw new Exception("Unable to connect to the database");
    }
    public function getStudentNotificationStatus($user_id) {
        $local = $this->conn;
        if($local != null) {
            $stmt = $local->prepare("SELECT notifications FROM students WHERE roll_no={$user_id}");
            $stmt->execute(); return $stmt->fetchAll()[0][0];
        } else throw new Exception("Unable to connect to the database");
    }
    
    public function findATeacher($user_id) {
        $local = $this->conn;
        if($local!=null) {
            $stmt = $local->prepare("SELECT * FROM teachers WHERE email='{$user_id}'");
            $stmt->execute(); $result = $stmt->fetchAll(); if(count($result)==0) return null; 
            $user = User::load($result[0]); $tid = $result[0]['TID']; if($user == null) return null;
            $stmt = $local->prepare("SELECT NAME, PID, SEC_GROUP,TYPE FROM papers 
                                     WHERE TID = {$tid} ORDER BY TYPE DESC, PID ASC");
            $stmt->execute(); $user->parsePapers($stmt->fetchAll()); return $user;
        } else throw new Exception("Unable to connect to database");
    }
    public function findTeacher($user_id, $MAC) {
        $local = $this->conn;
        if($local!=null) {
            $token = uniqid('user_',true);
            $local->exec("UPDATE teachers SET access_token='{$token}' WHERE email='{$user_id}'");
            $stmt = $local->prepare("SELECT * FROM teachers WHERE email='{$user_id}'");
            $stmt->execute(); $result = $stmt->fetchAll();
            if(count($result)==0) return null; $tMAC = $result[0]['LOGIN_MAC'];
            $stmt = $local->prepare("SELECT * FROM devices WHERE LOGIN_MAC='{$tMAC}'");
            $stmt->execute(); $device = $stmt->fetchAll(); $device=(count($device)!=0)?$device[0]:null;
            $user =  $tMAC==null || $device==null || ($device!=null && $device['ONLINE']==0)
                     ? User::load($result[0]) : null;
            $tid = $result[0]['TID']; if($user == null) return null;
            $stmt = $local->prepare("SELECT NAME, PID, SEC_GROUP, TYPE FROM papers 
                                     WHERE TID = {$tid} ORDER BY TYPE DESC, PID ASC");
            $stmt->execute(); $user->parsePapers($stmt->fetchAll()); return $user;
        } else throw new Exception("Unable to connect to database");
    }
    public function getTeacherDeviceToken($pid, $group, $type) {
        $local = $this->conn;
        if($local!=null) {
            $stmt = $local->prepare("SELECT LOGIN_MAC FROM teachers T JOIN papers P 
                                     ON T.TID = P.TID WHERE PID={$pid} AND 
                                     SEC_GROUP='{$group}' AND TYPE='{$type}'");
            $stmt->execute(); $result = $stmt->fetchAll();
            if(count($result)==0 || $result[0]["LOGIN_MAC"]==null) return null;
            return $this->getDeviceToken($result[0]["LOGIN_MAC"]);
        } else throw new Exception("Unable to connect to database");
    }
    public function getTeacherId($pid, $group, $type) {
        $local = $this->conn;
        if($local!=null) {
            $stmt = $local->prepare("SELECT EMAIL FROM teachers T JOIN papers P 
                                     ON T.TID = P.TID WHERE PID={$pid} AND 
                                     SEC_GROUP='{$group}' AND TYPE='{$type}'");
            $stmt->execute(); $result = $stmt->fetchAll();
            if(count($result)==0 || $result[0][0]==null) return null;
            return $result[0][0];
        } else throw new Exception("Unable to connect to database");
    }
    public function bindTeacherWithDevice($user_id, $MAC) {
        $local = $this->conn;
        if($local!=null) {
            $local->exec("UPDATE teachers SET LOGIN_MAC='{$MAC}' WHERE email='{$user_id}'");
            $stmt = $local->prepare("SELECT * FROM devices WHERE LOGIN_MAC='{$MAC}'");
            $stmt->execute(); $count = count($stmt->fetchAll());
            if($count == 0) { $local->exec("INSERT INTO devices(LOGIN_MAC) values ('{$MAC}')"); }
        } else throw new Exception("Unable to connect to database");
    }
    public function loginTeacher($user_id) {
        $local = $this->conn;
        if($local!=null) {
            $stmt = $local->prepare("SELECT LOGIN_MAC from teachers WHERE email='{$user_id}'");
            $stmt->execute(); $result = $stmt->fetchAll();
            if(count($result)==1) {
                $MAC = $result[0]['LOGIN_MAC'];
                if($MAC!=null) { $local->exec("UPDATE devices SET online=1 WHERE LOGIN_MAC='{$MAC}'"); return true; }
            }
        } else throw new Exception("Unable to connect to database");
        return false;
    }
    public function logoutTeacher($user_id, $clear = false) {
        $local = $this->conn;
        if($local!=null) {
            $stmt = $local->prepare("SELECT LOGIN_MAC from teachers WHERE email='{$user_id}'");
            $stmt->execute(); $result = $stmt->fetchAll();
            if(count($result)==1) {
                $MAC = $result[0]['LOGIN_MAC'];
                $local->exec("UPDATE devices SET online=0".($clear?",device_token=NULL":"")." WHERE LOGIN_MAC='{$MAC}'");
                if($clear) { $local->exec("UPDATE teachers SET LOGIN_MAC=NULL WHERE email='{$user_id}'"); }
            }
        } else throw new Exception("Unable to connect to database");
    }
    public function updateTeacherNotificationStatus($user_id, $notif) {
        $local = $this->conn;
        if($local != null) {
            $local->exec("UPDATE teachers SET notifications={$notif} WHERE email='{$user_id}'");
        } else throw new Exception("Unable to connect to the database");
    }
    public function getTeacherNotificationStatus($user_id) {
        $local = $this->conn;
        if($local != null) {
            $stmt = $local->prepare("SELECT notifications FROM teachers WHERE email='{$user_id}'");
            $stmt->execute(); return $stmt->fetchAll()[0][0];
        } else throw new Exception("Unable to connect to the database");
    }

    public function getDeviceToken($MAC) {
        $local = $this->conn;
        if($local!=null) {
            $stmt = $local->prepare("SELECT device_token FROM devices WHERE LOGIN_MAC='{$MAC}'");
            $stmt->execute(); $result = $stmt->fetchAll();
            if(count($result)==1) {
                print_r($result);
                return $result[0][0];
            } else return null;
        } else throw new Exception("Unable to connect to database");
    }
    public function setDeviceToken($MAC, $token) {
        $local = $this->conn;
        if($local!=null) {
            $local->exec("UPDATE devices SET device_token='{$token}',online=1 WHERE LOGIN_MAC='{$MAC}'");
        } else throw new Exception("Unable to connect to database");   
    }
    
    public function getPaper($pid) {
        $local = $this->conn;
        if($local!=null) {
            $stmt = $local->prepare("SELECT NAME FROM papers WHERE pid={$pid} LIMIT 1");
            $stmt->execute(); $result = $stmt->fetchAll();
            if(count($result)==0) return null;
            return $result[0]['NAME'];
        } else throw new Exception("Unable to connect to database");
    }
    public function getPaperCode($pname) {
        $local = $this->conn;
        if($local!=null) {
            $stmt = $local->prepare("SELECT PID FROM papers WHERE name='{$pname}' LIMIT 1");
            $stmt->execute(); $result = $stmt->fetchAll();
            if(count($result)==0) return null;
            return $result[0]['PID'];
        } else throw new Exception("Unable to connect to database");
    }
    public function getCourse($cid) {
        $local = $this->conn;
        if($local!=null) {
            $stmt = $local->prepare("SELECT NAME FROM course WHERE cid={$cid} LIMIT 1");
            $stmt->execute(); $result = $stmt->fetchAll();
            if(count($result)==0) return null;
            return $result[0]['NAME'];
        } else throw new Exception("Unable to connect to database");
    }
    
    public function addStudent($user) {
        $local = $this->conn;
        if($local!=null) {
            if(!isset($user->name)||!isset($user->rollNo)||!isset($user->courseId))
               throw new Exception("Incomplete request");
            $user->genUUID();
            $local->exec("INSERT INTO students VALUES({$user->rollNo},'{$user->name}'".
                     ",'{$user->accessToken}',{$user->courseId},'{$user->group}',NULL,1)");
            $stmt = $local->prepare("SELECT * FROM papers WHERE cid={$user->courseId} AND sec_group='{$user->group}'");
            $stmt->execute(); $result = $stmt->fetchAll(); $user->parsePapers($result);
            foreach($user->papers as $paper) {
                $pid = $paper['PID']; $type = $paper['TYPE'] ?? 'Theory';
                $local->exec("INSERT INTO attendance(roll_no, pid, sec_group, type) VALUES 
                             ({$user->rollNo},{$pid},'{$user->group}','{$type}')");
            }
        }  else throw new Exception("Unable to connect to database");
    }
    public function addTeacher($user) {
        $local = $this->conn;
        if($local!=null) {
            if(!isset($user->email)||!isset($user->name)||!isset($user->papers)||!isset($user->password))
               throw new Exception("Incomplete request");
            $stmt = $local->prepare("SELECT TID FROM teachers WHERE email='{$user->email}'"); 
            $stmt->execute(); $result = $stmt->fetchAll(); $ct = 0; $paper_set = "(NULL,NULL)";
            if(count($result)==0) {
                $stmt = $local->prepare("SELECT COUNT(TID) FROM teachers"); $stmt->execute();
                $ct = $stmt->fetchAll()[0][0]+1;
                $user->genUUID();
                $local->exec("INSERT INTO teachers VALUES('{$user->name}', '{$user->email}'".
                        ",'{$user->password}',{$ct},'{$user->accessToken}', NULL, 1)");
            } else { $ct = $result[0][0]; }
            foreach($user->papers as $paper) { 
                $paper_set .= ",({$paper['PID']},'{$paper['SEC_GROUP']}','{$paper['TYPE']}')"; 
            }
            $local->exec("UPDATE papers SET TID=$ct WHERE (pid,sec_group,type) IN ({$paper_set})");
        }  else throw new Exception("Unable to connect to database");
    }
    
    public function getAttendanceForPaper($pid, $group, $type) {
        $local = $this->conn;
        if($local != null) {
            $stmt = $local->prepare("SELECT * FROM attendance LIMIT 1"); $stmt->execute();
            $cols = "NAME"; 
            foreach($stmt->fetchAll(PDO::FETCH_ASSOC)[0] as $c=>$v) { 
                if(!in_array($c, ['PID', 'SEC_GROUP', 'TYPE'])) $cols .= ",{$c}"; }
            $stmt = $local->prepare("SELECT {$cols} FROM students NATURAL JOIN attendance 
                                     WHERE PID={$pid} AND SEC_GROUP='{$group}' AND TYPE='{$type}' ORDER BY name ASC");
            $stmt->execute(); $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $csv = ""; if(count($result)==0) return null;
            foreach($result[0] as $column=>$value) { $csv .= "{$column},"; } $csv .= "\n";
            foreach($result as $row) 
            { foreach($row as $name=>$value) { $csv .= "{$value},"; } $csv .= "\n"; }
            return $csv;
        } else throw new Exception("Unable to connect to the database.");
    }
    public function getAttendanceForRollNo($roll_no) {
        $local = $this->conn;
        if($local != null) {
            $stmt = $local->prepare("SELECT * FROM attendance LIMIT 1"); $stmt->execute();
            $cols = "PID"; $dates = "";
            foreach($stmt->fetchAll(PDO::FETCH_ASSOC)[0] as $column=>$value) {
                $cols .= ",{$column}"; 
                if(!in_array($column, [ "ROLL_NO", "PID", "SEC_GROUP", "TYPE" ])) 
                    $dates .= "{$column} ";
            }
            $dates = rtrim($dates);
            $stmt = $local->prepare("SELECT {$cols} FROM attendance WHERE roll_no={$roll_no}");
            $stmt->execute(); $result = $stmt->fetchAll(PDO::FETCH_ASSOC); 
            if(count($result)==0) return null;  
            return array("papers"=>$result, "dates"=>explode(' ',$dates));
        } else throw new Exception("Unable to connect to the database.");
    }

    public function addRequest($request, $checkValidityFirst = true) {
        $local = $this->conn;
        if($local != null) {
            if($checkValidityFirst) {
                $stmt = $local->prepare("SELECT * FROM students S JOIN papers P ON P.CID=S.CID 
                                     AND P.SEC_GROUP=S.SEC_GROUP WHERE ROLL_NO={$request->rollNo} 
                                     AND PID={$request->paperId} AND P.SEC_GROUP='{$request->group}' 
                                     AND P.TYPE='{$request->type}'");
                $stmt->execute(); if(count($stmt->fetchAll()) == 0) return false;
            }
            $local->exec("INSERT INTO requests VALUES({$request->rollNo},{$request->creator},
                         {$request->paperId},'{$request->group}','{$request->type}',
                         {$request->approved},{$request->handled},SYSDATE())");
            return true;
        } else throw new Exception("Unable to connect to the database");
    }
    public function updateRequest($request) {
        date_default_timezone_set("Asia/Kolkata");
        $local = $this->conn;
        if($local != null) {
            $reqdate = date('Y-m-d');
            $local->exec("UPDATE requests SET approved={$request->approved}, handled={$request->handled} 
                          WHERE pid={$request->paperId} AND roll_no={$request->rollNo} 
                          AND SEC_GROUP='{$request->group}' AND TYPE='{$request->type}' AND date='{$reqdate}'");
            $stmt = $local->prepare("SELECT * FROM requests WHERE pid={$request->paperId} 
                                     AND roll_no={$request->rollNo} AND date='{$reqdate}' 
                                     AND SEC_GROUP='{$request->group}' AND TYPE = '{$request->type}'");
            $stmt->execute(); $result = $stmt->fetchAll();
            if(count($result)!=0) {
                $req = Request::from($result[0]);
                if($req->handled==1 && $req->date != null) {
                    $date = date_format($req->date, "d_m_Y");
                    try {
                        $local->exec("ALTER TABLE attendance ADD {$date} TEXT NOT NULL DEFAULT '-'");
                    } catch(PDOException $e) {}
                    $local->exec("UPDATE attendance SET {$date}='A' WHERE {$date}='-' AND 
                                  pid={$request->paperId} AND SEC_GROUP='{$request->group}'
                                  AND TYPE='{$request->type}'");
                    $mark = $req->approved==1 ? 'P' : 'A';
                    $local->exec("UPDATE attendance SET {$date}='{$mark}' WHERE 
                                  roll_no={$req->rollNo} AND pid={$req->paperId} 
                                  AND SEC_GROUP='{$request->group}' AND TYPE='{$request->type}'");
                }
            }             
        } else throw new Exception("Unable to connect to the database");
    }
    public function generateRequests($pid, $group, $type, $paper = null) {
        $local = $this->conn;
        if($local != null) {
            $stmt = $local->prepare("SELECT ROLL_NO FROM students S NATURAL JOIN 
                                     DEVICES WHERE DEVICE_TOKEN IS NOT NULL AND EXISTS (SELECT PID FROM 
                                     papers P WHERE P.SEC_GROUP=S.SEC_GROUP AND P.CID=S.CID 
                                     AND PID={$pid} AND P.SEC_GROUP='{$group}' AND P.TYPE='{$type}') AND NOT EXISTS 
                                     (SELECT * FROM requests R WHERE R.ROLL_NO=S.ROLL_NO AND 
                                     R.PID={$pid} AND R.TYPE='{$type}' AND DATEDIFF(DATE,SYSDATE())=0)");
            $stmt->execute(); $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(count($result) > 0) {
                foreach($result as $row) {
                    $row += ["CREATOR"=>$row['ROLL_NO'],
                             "SEC_GROUP"=>$group,
                             "PAPER"=>$paper ?? $this->getPaper($pid),
                             "PID"=>$pid,
                             "TYPE"=>$type];
                    $this->addRequest(Request::from($row), false);
                }
            }
        } else throw new Exception("Unable to connect to the database");
    }
    public function getRequestsByCreator($creator) {
        $local = $this->conn; 
        date_default_timezone_set('Asia/Kolkata'); $reqdate = date('Y-m-d');
        if($local != null) {
            $stmt = $local->prepare("SELECT CREATOR, S.roll_no AS ROLL_NO, APPROVED, HANDLED, 
                                     S.SEC_GROUP AS SEC_GROUP, R.TYPE AS TYPE, P.PID AS PID, P.NAME AS PAPER 
                                     FROM requests R, papers P, students S WHERE date='{$reqdate}'
                                     AND S.roll_no=R.roll_no AND P.pid=R.pid AND handled = 0
                                     AND creator={$creator} AND S.SEC_GROUP=P.SEC_GROUP 
                                     AND R.TYPE=P.TYPE");
            $stmt->execute(); return $stmt->fetchAll(PDO::FETCH_ASSOC); 
        } else throw new Exception("Unable to connect to the database");
    }
    public function getRequestsForPaper($pid, $group, $type) {
        $local = $this->conn;
        date_default_timezone_set('Asia/Kolkata'); $reqdate = date('Y-m-d');
        if($local != null) {
            $stmt = $local->prepare("SELECT S.NAME AS NAME, CREATOR, S.ROLL_NO AS ROLL_NO, 
                                    P.NAME AS PAPER, R.PID AS PID, R.TYPE AS TYPE, 
                                    R.SEC_GROUP AS SEC_GROUP, APPROVED, HANDLED 
                                    FROM students S, requests R, papers P 
                                    WHERE R.SEC_GROUP=S.SEC_GROUP AND S.ROLL_NO=R.ROLL_NO 
                                    AND P.PID=R.PID AND R.TYPE=P.TYPE AND HANDLED = 0 
                                    AND P.SEC_GROUP=R.SEC_GROUP AND DATE='{$reqdate}' 
                                    AND R.PID={$pid} AND R.SEC_GROUP='{$group}' 
                                    AND R.TYPE='{$type}' ORDER BY NAME");
            $stmt->execute(); return $stmt->fetchAll(PDO::FETCH_ASSOC); 
        } else throw new Exception("Unable to connect to the database");
    }
}

?>