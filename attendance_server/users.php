<?php

require_once("database.php");

try {
    $db = new Database();
    if($_SERVER['REQUEST_METHOD']=='GET') {
        $auth = getAuthorization();
        if($auth!=null) {
            $user_id = $auth['user_id']; $access_token = $auth['access_token'];
            if(isEmail($user_id)) {
                $user = $db->findATeacher($user_id);
                if($user!=null && $user->accessToken=$access_token) {
                    if(isset($_GET['login'])) {
                        if(!$db->loginTeacher($user_id)) { http_response_code(412); return; }
                    } else if(isset($_GET['logout'])) {
                        $db->logoutTeacher($user_id, isset($_GET['clear'])?true:false);
                    } else { http_response_code(400); return; }
                    http_response_code(200); return;
                } else { http_response_code(401); return; }
            } else {
                $user = $db->findAStudent($user_id, true);
                if($user!=null && $user->accessToken=$access_token) {
                    if(isset($_GET['login'])) {
                        if(!$db->loginStudent($user_id)) { http_response_code(412); return; }
                    } else if(isset($_GET['logout'])) {
                        $db->logoutStudent($user_id, isset($_GET['clear'])?true:false);
                    } else { http_response_code(400); return; }
                    http_response_code(200); return;
                } else { http_response_code(401); return; }
            }
        } else if(allSet($_GET, 'user_id', 'MAC')) {
            $MAC = $_GET['MAC']; $user_id = $_GET['user_id']; $user_pw = "";
            if(isset($_GET['user_pw'])) $user_pw = $_GET['user_pw'];
            if(isEmail($user_id)) {
                $user = $db->findTeacher($user_id, $MAC);
                if($user!=null && $user->password == $user_pw) {
                    $db->bindTeacherWithDevice($user_id, $MAC);
                    print_r(json_encode(array("name"=>$user->name, 
                                              "type"=>"Teacher", 
                                              "user_id"=>$user->email, 
                                              "access_token"=>$user->accessToken,
                                              "papers"=>$user->papers)));
                    http_response_code(200);
                } else if($user==null) http_response_code(403); 
                else http_response_code(401);
            } else {
                $user = $db->findStudent($user_id, $MAC);
                if($user!=null) {
                    $db->bindStudentWithDevice($user_id, $MAC);
                    $course = $db->getCourse($user->courseId);
                    print_r(json_encode(array("name"=>$user->name, 
                                              "type"=>"Student",
                                              "user_id"=>$user->rollNo, 
                                              "access_token"=>$user->accessToken,
                                              "papers"=>$user->papers,
                                              "course"=>$course,
                                              "SEC_GROUP"=>$user->group )));
                    http_response_code(200);
                } else http_response_code(403); 
            }
        } else { http_response_code(400); }
    } else if($_SERVER['REQUEST_METHOD']=='POST') {
        $auth = getAuthorization("Bearer");
        if($auth!=null && $auth['access_token']==ADMIN_KEY) {
            print_r($_POST);
            $array = json_decode(file_get_contents("php://input"), true);
            if($array == null) $array = $_REQUEST;
            $user = User::load($array);
            if($user == null) { http_response_code(400); return; }
            if(isset($user->rollNo)) $db->addStudent($user); else $db->addTeacher($user);
            http_response_code(200);
        } else if(($auth = getAuthorization())!=null) {
            $notifications = $_POST['notifications'] ?? null;
            $user_id = $auth['user_id']; $access_token = $auth['access_token'];
            if(isEmail($user_id)) {
                $user = $db->findATeacher($user_id);
                if($user!=null && $user->accessToken==$access_token) {
                    if($notifications!=null) {
                        $db->updateTeacherNotificationStatus($user->email, $notifications);
                        http_response_code(200);
                    } else http_response_code(400);
                } else http_response_code(401);
            } else {
                $user = $db->findAStudent($user_id, true);
                if($user!=null && $user->accessToken==$access_token) {
                    if($notifications!=null) {
						echo $user->rollNo . " => " . $notifications;
                        $db->updateStudentNotificationStatus($user->rollNo, $notifications);
                        http_response_code(200);
                    } else http_response_code(400);
                } else http_response_code(401);
            }
        } else if(allSet($_POST, 'MAC', 'device_token')) {
            $MAC = $_POST['MAC']; $token = $_POST['device_token'];
            $db->setDeviceToken($MAC, $token); http_response_code(200); return;
        } else { http_response_code(400); } 
    }
}
catch(Exception $e) { http_response_code(418); print_r($e->getMessage()); } 

?>