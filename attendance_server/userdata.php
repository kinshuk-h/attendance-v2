<?php

require_once("database.php");

try {
    $db = new Database();
    if(allSet($_GET, 'PID', 'SEC_GROUP', 'TYPE')) {
        $pid = $_GET['PID']; $group = $_GET['SEC_GROUP']; $type = $_GET['TYPE'];
        $csv = $db->getAttendanceForPaper($pid, $group, $type);
        if($csv!=null) { http_response_code(200); print_r($csv); }
        else http_response_code(400);
    } else {
        $roll_no = $_GET['ROLL_NO'] ?? 'NULL';
        $data = $db->getAttendanceForRollNo($roll_no);
        if($data!=null) { http_response_code(200); print_r(json_encode($data)); }
        else http_response_code(400);
    }
} catch(Exception $e) { http_response_code(418); print_r($e->getMessage()); }

?>