package in.ac.du.keshav.attendance.UI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.ac.du.keshav.attendance.R;

public class FlexibleTableAdapter extends RecyclerView.Adapter<FlexibleTableAdapter.ViewHolder> {
    private Context context;
    private ArrayList<FlexibleTableColumnAdapter> table = new ArrayList<>();

    public FlexibleTableAdapter(Context context) {
        this.context = context;
    }

    @NonNull @Override public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_flex_table_column, parent, false));
    }
    @Override public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.column.setLayoutManager(new NonScrollableLinearLayoutManager(context, RecyclerView.VERTICAL, false));
        holder.column.setAdapter(table.get(position));
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        RecyclerView column;
        ViewHolder(View root) {
            super(root); this.column = (RecyclerView) root;
        }
    }

    @Override public int getItemCount() { return table.size(); }
    public void removeColumn(int position) { table.remove(position); notifyItemRemoved(position); }
    public ArrayList<String> getColumn(int position) { return table.get(position).getAll(); }
    //public String getCell(int x, int y) { return table.get(y).get(x); }
    public void addColumn(ArrayList<String> column, boolean highlighted) {
        FlexibleTableColumnAdapter adapter = new FlexibleTableColumnAdapter(context, highlighted);
        adapter.addAll(column); table.add(adapter);
        if(table.size()>1 && table.get(0).getItemCount() != adapter.getItemCount()) {
            int diff = table.get(0).getItemCount() - adapter.getItemCount();
            if(diff < 0) {
                diff = -diff;
                for(int i=0;i<getItemCount()-1;i++) {
                    for(int j=0;j<diff;j++) table.get(i).add(" ");
                }
            } else {
                for(int j=0;j<diff;j++) adapter.add(" ");
            }
        }
        notifyItemInserted(table.size()-1);
    }
    public void add(boolean highlighted, String... new_row) {
        if(table.size()>1) {
            int diff = getItemCount() - new_row.length;
            if(diff<0) {
                diff = -diff; int old_size = table.get(0).getItemCount();
                for(int i=0;i<diff;i++) {
                    FlexibleTableColumnAdapter newAdapter = new FlexibleTableColumnAdapter(context, highlighted);
                    for(int j=0;j<old_size;j++) newAdapter.add(" "); table.add(newAdapter);
                }
                int i = 0; for(String s : new_row) { table.get(i).add(s); }
            } else if(diff>0) {
                int i=0; for(String s : new_row) { table.get(i++).add(s); }
                for(;i<getItemCount();i++) { table.get(i).add(""); }
            } else { int i=0; for(String s : new_row) { table.get(i++).add(s); } }
        } else {
            for (String s : new_row) {
                FlexibleTableColumnAdapter adapter = new FlexibleTableColumnAdapter(context, highlighted);
                adapter.add(s); table.add(adapter);
            }
        }
        notifyDataSetChanged();
    }
    public void addColumn(ArrayList<String> column) { addColumn(column, false); }
    public void add(String... row) { add(false, row); }
    public void clear() { table.clear(); notifyDataSetChanged(); }
}
