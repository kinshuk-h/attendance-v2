package in.ac.du.keshav.attendance.utilities;

// Holds some common constants to be used across classes.
public interface Constants {
    // REGEX patterns.
    interface Regex {
        String EMAIL_REGEX = "[A-Za-z0-9._]+@keshav.du.ac.in";
        String ROLL_NO_REGEX = "\\d{4}";
    }
    // Intent & Volley Request Data keys.
    interface Data {
        String NAME          = "name"        ;
        String USER          = "user"        ;
        String MAP           = "data_map"    ;
        String SOURCE        = "source"      ;
        String COURSE        = "course"      ;
        String USER_TYPE     = "type"        ;
        String DATES         = "dates"       ;
        String PAPERS        = "papers"      ;
        String USER_ID       = "user_id"     ;
        String USER_PASSWORD = "user_pw"     ;
        String MAC_ADDRESS   = "MAC"         ;
        String DEVICE_TOKEN  = "device_token";
        String ACCESS_TOKEN  = "access_token";
        String REQUEST_NAME  = "NAME"        ;
        String PAPER         = "PAPER"       ;
        String PAPER_ID      = "PID"         ;
        String GROUP         = "SEC_GROUP"   ;
        String ROLL_NO       = "ROLL_NO"     ;
        String CREATOR       = "CREATOR"     ;
        String HANDLED       = "HANDLED"     ;
        String APPROVED      = "APPROVED"    ;
        String PAPER_TYPE    = "TYPE"        ;
    }
    // Header Fields.
    interface Headers {
        String AUTHORIZATION = "Authorization";
        String CONTENT_TYPE = "Content-Type";
    }
    // Request URLs.
    interface URL {
        String Base = "https://keshav.sytes.net/sites/sec-a/kinshuk/attendance_server";
        String Requests = Base+"/request.php";
        String Users = Base+"/users.php";
        String Userdata = Base+"/userdata.php";
    }
    // Shared Preference keys.
    interface Preferences {
        String NOTIFICATIONS = "notifications";
        String LOGOUT = "logout";
        String VERSION = "session_version";
        int DEFAULT_VERSION = 1;
    }
    // Request codes.
    interface Requests {
        int SAVE_CSV_MONTH = 690;
        int SAVE_CSV_ALL = 691;
    }
    // Intent actions
    interface Actions {
        String NEW_TOKEN = "new_token";
        String NEW_MESSAGE = "new_message";
    }
    int RETRY_ATTEMPT_COUNT = 3;
}
