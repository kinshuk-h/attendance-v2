package in.ac.du.keshav.attendance.UI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.ac.du.keshav.attendance.R;
import in.ac.du.keshav.attendance.data.PaperMetrics;

public class TableRowAdapter extends RecyclerView.Adapter<TableRowAdapter.ViewHolder> {
    private ArrayList<Pair<String, ArrayList<PaperMetrics.Count>>> rows = new ArrayList<>();
    private Context context;

    TableRowAdapter(Context context) {
        this.context = context;
    }

    @NonNull @Override public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_table_row, parent, false));
    }
    @Override public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Pair<String, ArrayList<PaperMetrics.Count>> item = rows.get(position);
        holder.month.setText(item.first); assert item.second != null;
        holder.current_theory.setText(String.valueOf(item.second.get(0).current));
        holder.current_practical.setText(String.valueOf(item.second.get(1).current));
        holder.current_tutorial.setText(String.valueOf(item.second.get(2).current));
        holder.total_theory.setText(String.valueOf(item.second.get(0).total));
        holder.total_practical.setText(String.valueOf(item.second.get(1).total));
        holder.total_tutorial.setText(String.valueOf(item.second.get(2).total));
    }
    @Override public int getItemCount() { return rows.size(); }

    static class ViewHolder extends RecyclerView.ViewHolder {
        View root; TextView month;
        TextView current_theory, current_practical, current_tutorial,
                 total_theory, total_practical, total_tutorial;
        ViewHolder(View root) {
            super(root); this.root = root;
            month = root.findViewById(R.id.month);
            current_theory = root.findViewById(R.id.current_theory);
            total_theory = root.findViewById(R.id.total_theory);
            current_practical = root.findViewById(R.id.current_practical);
            total_practical = root.findViewById(R.id.total_practical);
            current_tutorial = root.findViewById(R.id.current_tutorial);
            total_tutorial = root.findViewById(R.id.total_tutorial);
        }
    }

    //public Pair<String, ArrayList<PaperMetrics.Count>> getItem(int position) { return rows.get(position); }
    public void add(String month, ArrayList<PaperMetrics.Count> count) {
        rows.add(new Pair<>(month, count)); notifyItemInserted(rows.size()-1);
    }
}