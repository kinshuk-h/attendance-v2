package in.ac.du.keshav.attendance.UI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Map;

import in.ac.du.keshav.attendance.R;
import in.ac.du.keshav.attendance.data.PaperMetrics;

public class TableAdapter extends RecyclerView.Adapter<TableAdapter.ViewHolder> {
    private ArrayList<PaperMetrics> papers = new ArrayList<>();
    private Context context;

    public TableAdapter(Context context) {
        this.context = context;
    }

    @NonNull @Override public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_paper_table, parent, false));
    }
    @Override public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PaperMetrics paper = papers.get(position); holder.name.setText(paper.name);

        int warning = R.drawable.ic_warning;
        ArrayList<PaperMetrics.Count> cc = paper.getCumulative();
        if(cc.get(0).total == 0) holder.percent_theory.setText("--%");
        else {
            double percent = (double) cc.get(0).current / cc.get(0).total;
            holder.percent_theory.setText((new DecimalFormat("#0.00%").format(percent)));
            if(percent < 0.6) holder.percent_theory.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0, warning,0);
        }
        if(cc.get(1).total == 0) holder.percent_practical.setText("--%");
        else {
            double percent = (double) cc.get(1).current / cc.get(1).total;
            holder.percent_practical.setText((new DecimalFormat("#0.00%").format(percent)));
            if(percent < 0.6) holder.percent_practical.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0, warning,0);
        }
        if(cc.get(2).total == 0) holder.percent_tutorial.setText("--%");
        else {
            double percent = (double) cc.get(2).current / cc.get(2).total;
            holder.percent_tutorial.setText((new DecimalFormat("#0.00%").format(percent)));
            if(percent < 0.6) holder.percent_tutorial.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0, warning,0);
        }

        TableRowAdapter adapter = new TableRowAdapter(context);
        for(Map.Entry<String, ArrayList<PaperMetrics.Count>> pair : paper.records.entrySet()) {
            adapter.add(pair.getKey(), pair.getValue());
        } holder.rows.setAdapter(adapter);

        holder.current_theory.setText(String.valueOf(cc.get(0).current));
        holder.total_theory.setText(String.valueOf(cc.get(0).total));
        holder.current_practical.setText(String.valueOf(cc.get(1).current));
        holder.total_practical.setText(String.valueOf(cc.get(1).total));
        holder.current_tutorial.setText(String.valueOf(cc.get(2).current));
        holder.total_tutorial.setText(String.valueOf(cc.get(2).total));
    }
    @Override public int getItemCount() { return papers.size(); }

    class ViewHolder extends RecyclerView.ViewHolder {
        View root; TextView percent_theory, percent_practical, percent_tutorial, name; RecyclerView rows;
        TextView current_theory, current_practical, current_tutorial, total_theory, total_practical, total_tutorial;
        ViewHolder(View root) {
            super(root); this.root = root;
            percent_theory = root.findViewById(R.id.percentage_theory);
            percent_practical = root.findViewById(R.id.percentage_practical);
            percent_tutorial = root.findViewById(R.id.percentage_tutorial);
            current_theory = root.findViewById(R.id.current_theory);
            total_theory = root.findViewById(R.id.total_theory);
            current_practical = root.findViewById(R.id.current_practical);
            total_practical = root.findViewById(R.id.total_practical);
            current_tutorial = root.findViewById(R.id.current_tutorial);
            total_tutorial = root.findViewById(R.id.total_tutorial);
            name = root.findViewById(R.id.paper);
            rows = root.findViewById(R.id.month_list);
            rows.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        }
    }

    //public PaperMetrics getItem(int position) { return papers.get(position); }
    public void add(PaperMetrics paper) {
        papers.add(paper); notifyItemInserted(papers.size()-1);
    }
}
