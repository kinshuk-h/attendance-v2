package in.ac.du.keshav.attendance.UI;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;

import in.ac.du.keshav.attendance.R;

public class FlexibleTableRowAdapter extends RecyclerView.Adapter<FlexibleTableRowAdapter.ViewHolder> {
    private boolean isHeader;

    private Context context;
    private ArrayList<String> row = new ArrayList<>();
    private ArrayList<Integer> sizes = new ArrayList<>();

    public FlexibleTableRowAdapter(Context context, boolean isHeader) {
        this.context = context; this.isHeader = isHeader;
    }

    @NonNull @Override public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(
                isHeader?R.layout.item_flex_table_header:R.layout.item_flex_table_cell, parent, false));
    }
    @Override public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //int size, csize; try { size = sizes.get(position); } catch (Exception e) { size = 0; }
        holder.field.setText(row.get(position));
        //holder.field.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        //csize = holder.field.getMeasuredWidth();
        //if(size>0) { if(csize>size) sizes.set(position, csize); else holder.field.setWidth(size); }
        //else { sizes.set(position, csize); }
    }

    ArrayList<Integer> getFieldSizes() { return sizes; }
    void resizeFields(ArrayList<Integer> sizes) {
        this.sizes = sizes; notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView field;
        ViewHolder(View root) { super(root); field = (TextView) root; }
    }

    @Override public int getItemCount() { return row.size(); }
    public String get(int position) { return row.get(position); }
    public ArrayList<String> getAll() { return row; }
    public void add(String value) { row.add(value); sizes.add(1); notifyItemInserted(row.size()-1); }
    public void addAll(String... values) { for(String value : values) add(value); }
}
