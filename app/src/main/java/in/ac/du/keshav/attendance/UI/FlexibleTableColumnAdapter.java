package in.ac.du.keshav.attendance.UI;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;

import in.ac.du.keshav.attendance.R;

public class FlexibleTableColumnAdapter extends RecyclerView.Adapter<FlexibleTableColumnAdapter.ViewHolder> {
    private Context context; private boolean highlighted;
    private ArrayList<String> column = new ArrayList<>();

    FlexibleTableColumnAdapter(Context context, boolean highlighted) { this.highlighted = highlighted; this.context = context; }

    @Override public int getItemViewType(int position) {
        return position==0?R.layout.item_flex_table_header:R.layout.item_flex_table_cell;
    }

    @NonNull @Override public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(viewType, parent, false));
    }
    @Override public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.field.setText(get(position));
        if(getItemViewType(position) == R.layout.item_flex_table_cell) {
            if(highlighted) {
                holder.field.setBackgroundColor(context.getResources().getColor(position % 2 != 0 ? R.color.tableHighlightedOddRow : R.color.tableHighlightedEvenRow));
                holder.field.setGravity(Gravity.CENTER);
            }
            else {
                holder.field.setBackgroundColor(context.getResources().getColor(position % 2 != 0 ? R.color.tableOddRow : R.color.tableEvenRow));
                holder.field.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
            }
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView field;
        ViewHolder(View root) { super(root); field = (TextView) root; }
    }

    @Override public int getItemCount() { return column.size(); }
    public String get(int position) { return column.get(position); }
    ArrayList<String> getAll() { return column; }
    public void add(String value) { column.add(value); notifyItemInserted(column.size()-1); }
    void addAll(Collection<? extends String> values) { column.addAll(values); notifyDataSetChanged(); }
}
