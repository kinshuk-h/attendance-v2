package in.ac.du.keshav.attendance.data;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {
    public String name;
    private String accessToken;
    public ArrayList<Paper> papers = new ArrayList<>();
    public Paper currentPaper = null;

    public String getAccessToken() { return accessToken; }
    public void setAccessToken(String accessToken) { this.accessToken = accessToken; }
    public String getUserId() { return null; }
    public String getType() { return null; }
}
