package in.ac.du.keshav.attendance.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import in.ac.du.keshav.attendance.R;
import in.ac.du.keshav.attendance.UI.FlexibleTableAdapter;
import in.ac.du.keshav.attendance.activities.MainActivity;
import in.ac.du.keshav.attendance.data.Paper;
import in.ac.du.keshav.attendance.utilities.Constants;
import in.ac.du.keshav.attendance.utilities.CoreUtils;

public class TeacherAttendanceFragment extends Fragment {
    private static final String TAG = TeacherAttendanceFragment.class.getName();

    private ProgressBar progress;
    private TextView noRecords;
    private RecyclerView recyclerView;
    private Spinner paperList, monthList;
    private FloatingActionButton download, download_all, download_month;
    private NestedScrollView scrollView;
    private LinearLayout action_month, action_total;

    private ArrayList<ArrayList<String>> columns;
    private ArrayList<Integer> positions = new ArrayList<>();
    private FlexibleTableAdapter adapter;

    private MainActivity activity;
    private View root;
    private boolean isMenuOpen = false;

    private void showMenu() {
        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f); animation.setDuration(1000);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override public void onAnimationStart(Animation animation)
            { download.setEnabled(false); action_month.setVisibility(View.VISIBLE); action_total.setVisibility(View.VISIBLE); isMenuOpen = false; }
            @Override public void onAnimationEnd(Animation animation) { download.setEnabled(true); isMenuOpen = true; }
            @Override public void onAnimationRepeat(Animation animation) { }
        });
        action_month.startAnimation(animation); action_total.startAnimation(animation);
    }
    private void hideMenu(final boolean withButton) {
        AlphaAnimation animation = new AlphaAnimation(1.0f, 0.0f); animation.setDuration(1000);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override public void onAnimationStart(Animation animation) { download.setEnabled(false); isMenuOpen = false; }
            @Override public void onAnimationEnd(Animation animation) {
                action_month.setVisibility(View.GONE); action_total.setVisibility(View.GONE);
                download.setEnabled(true); isMenuOpen = false; if(withButton) download.hide();
            }
            @Override public void onAnimationRepeat(Animation animation) { }
        });
        action_month.startAnimation(animation); action_total.startAnimation(animation);
    }

    private void setupMonths() {
        final ArrayList<String> months = new ArrayList<>(); positions.clear();
        for(int i=2;i<columns.size();i++) {
            String name = CoreUtils.getMonth(columns.get(i).get(0));
            if(months.isEmpty() || !months.get(months.size()-1).equals(name)) { months.add(name); positions.add(i); }
        }
        positions.add(columns.size());
        Log.i(TAG, "Positions: "+positions);

        ArrayAdapter monthAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, months);
        monthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); monthList.setAdapter(monthAdapter);
        monthList.setVisibility(View.VISIBLE); recyclerView.setVisibility(View.VISIBLE);
        progress.setVisibility(View.GONE); download.show();

        monthList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ArrayList<String> total = new ArrayList<>(columns.get(0).size());
                    total.add("TA ("+(positions.get(position+1)-positions.get(position))+")");
                    for(int i=1; i<columns.get(0).size(); i++) total.add("0");
                    adapter.clear(); adapter.addColumn(columns.get(0)); adapter.addColumn(columns.get(1));
                    for(int i=positions.get(position);i<positions.get(position+1);i++) {
                        adapter.addColumn(columns.get(i));
                        for(int j=1; j<columns.get(i).size(); j++)
                            if(columns.get(i).get(j).equals("P"))
                                total.set(j, String.valueOf(Integer.parseInt(total.get(j))+1));
                    }
                    adapter.addColumn(total, true);
                } catch (Exception e) { Log.e(TAG, "error: ", e); }
            }
            @Override public void onNothingSelected(AdapterView<?> parent) { }
        });
    }
    private void fetchAttendance(Paper paper) {
        Map<String, String> map = new HashMap<>();
        map.put(Constants.Data.PAPER_ID, paper.getPaperId());
        map.put(Constants.Data.GROUP, paper.getGroup());
        map.put(Constants.Data.PAPER_TYPE, paper.getType());

        progress.setVisibility(View.VISIBLE); if(isMenuOpen) hideMenu(true); else download.hide();
        monthList.setVisibility(View.GONE); noRecords.setVisibility(View.GONE); recyclerView.setVisibility(View.GONE);

        StringRequest request = new StringRequest(
                Constants.URL.Userdata + "?" + CoreUtils.toQueryString(map),
                new Response.Listener<String>() {
                    @Override public void onResponse(String response) {
                        String [] rows = response.split("\n");
                        adapter = new FlexibleTableAdapter(activity);

                        String[] headers = rows[0].split(",");
                        headers[1] = headers[1].replace('_', ' ');
                        for(int i=2;i<headers.length;i++) {
                            headers[i] = headers[i].replace('_','/');
                            headers[i] = headers[i].substring(0,headers[i].lastIndexOf('/'));
                        }
                        adapter.add(headers);
                        for(int i=1;i<rows.length;i++) { adapter.add(rows[i].split(",")); }

                        columns = new ArrayList<>();
                        for(int i=0;i<adapter.getItemCount();i++) {
                            if(adapter.getColumn(i).get(1).equals("-")) { adapter.removeColumn(i--); }
                            else columns.add(adapter.getColumn(i));
                        }

                        recyclerView.setAdapter(adapter);

                        if(adapter.getItemCount()<3) {
                            progress.setVisibility(View.GONE); noRecords.setVisibility(View.VISIBLE);
                            if(isMenuOpen) { hideMenu(true); } else download.hide();
                        }
                        else setupMonths();
                    }
                },
                new Response.ErrorListener() {
                    @Override public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "error: ", error);
                        if(error.networkResponse != null) {
                            Log.e(TAG, "Status Code: "+error.networkResponse.statusCode);
                            Log.e(TAG, "Response: "+CoreUtils.toString(error.networkResponse.data));
                        }
                        progress.setVisibility(View.GONE); noRecords.setVisibility(View.VISIBLE);
                        if(isMenuOpen) { hideMenu(true); } else download.hide();
                    }
                });
        activity.getQueue().add(request);
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstance) {
        return inflater.inflate(R.layout.fragment_attendance_teacher, parent, false);
    }
    @Override public void onActivityCreated(Bundle savedInstance) {
        super.onActivityCreated(savedInstance);
        activity = (MainActivity) getActivity(); root = getView();
        final SwipeRefreshLayout layout = (SwipeRefreshLayout) root;

        if(root!=null) {
            recyclerView = root.findViewById(R.id.recycler_view);
            progress = root.findViewById(R.id.progress);
            noRecords = root.findViewById(R.id.no_records);
            paperList = root.findViewById(R.id.paper);
            monthList = root.findViewById(R.id.month);
            download = root.findViewById(R.id.btn_download);
            scrollView = root.findViewById(R.id.scroll_view);
            download_all = root.findViewById(R.id.btn_download_all);
            download_month = root.findViewById(R.id.btn_download_month);
            action_month = root.findViewById(R.id.action_month);
            action_total = root.findViewById(R.id.action_total);
        }
        if(activity!=null && layout != null) {
            download.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    if(isMenuOpen) { hideMenu(false); } else { showMenu(); }
                }
            }); download.hide();

            final ArrayList<String> papers = new ArrayList<>(); for(Paper p : activity.getUser().papers) { papers.add(p.getCompleteName()); }
            final ArrayAdapter paperAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, papers);
            paperAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); paperList.setAdapter(paperAdapter);

            layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override public void onRefresh() {
                    Paper paper = activity.getUser().papers.get(paperList.getSelectedItemPosition());
                    layout.setRefreshing(true); fetchAttendance(paper); layout.setRefreshing(false);
                }
            });

            scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    int dy = scrollY - oldScrollY;
                    if (dy > 10 && download.getVisibility() == View.VISIBLE) { if(isMenuOpen) { hideMenu(true); } else download.hide(); }
                    else if (dy < -10 && download.getVisibility() != View.VISIBLE) { download.show(); }
                }
            });
            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    if (dx > 10 && download.getVisibility() == View.VISIBLE) { if(isMenuOpen) { hideMenu(true); } else download.hide(); }
                    else if (dx < -10 && download.getVisibility() != View.VISIBLE) { download.show(); }
                }
            });

            download_all.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    if(isMenuOpen) {
                        if(columns!=null) {
                            Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
                            intent.addCategory(Intent.CATEGORY_OPENABLE); intent.setType("text/csv");
                            intent.putExtra(Intent.EXTRA_TITLE, "Attendance - "+paperList.getSelectedItem().toString()+".csv");
                            if(intent.resolveActivity(activity.getPackageManager())!=null) {
                                startActivityForResult(intent, Constants.Requests.SAVE_CSV_ALL);
                            }
                            else Snackbar.make(root, "Save Failed", Snackbar.LENGTH_SHORT).show();
                        }
                        else Toast.makeText(activity.getApplicationContext(), R.string.save_error, Toast.LENGTH_SHORT).show();
                    }
                }
            });
            download_month.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    if(isMenuOpen) {
                        if(columns!=null && monthList!=null) {
                            Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
                            intent.addCategory(Intent.CATEGORY_OPENABLE); intent.setType("text/csv");
                            intent.putExtra(Intent.EXTRA_TITLE, "Attendance - "+paperList.getSelectedItem().toString()+
                                            " - "+monthList.getSelectedItem().toString()+".csv");
                            if(intent.resolveActivity(activity.getPackageManager())!=null) {
                                startActivityForResult(intent, Constants.Requests.SAVE_CSV_MONTH);
                            }
                            else Snackbar.make(root, "Save Failed", Snackbar.LENGTH_SHORT).show();
                        }
                        else Toast.makeText(activity.getApplicationContext(), R.string.save_error, Toast.LENGTH_SHORT).show();
                    }
                }
            });

            paperList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override public void onItemSelected(final AdapterView<?> parent, View view, final int position, long id) {
                    parent.setEnabled(false);
                    new Timer().schedule(new TimerTask() {
                        @Override public void run() {
                            activity.runOnUiThread(new Runnable() {
                                @Override public void run() {
                                    parent.setEnabled(true);
                                    fetchAttendance(activity.getUser().papers.get(position));
                                }
                            });
                        }
                    }, 300);
                }
                @Override public void onNothingSelected(AdapterView<?> parent) {
                    progress.setVisibility(View.GONE); noRecords.setVisibility(View.VISIBLE);
                }
            });
        }
    }
    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == AppCompatActivity.RESULT_OK &&
           (requestCode==Constants.Requests.SAVE_CSV_ALL || requestCode==Constants.Requests.SAVE_CSV_MONTH)) {
            if(data!=null) {
                Uri path = data.getData();
                if(path!=null) {
                    try {
                        ParcelFileDescriptor pfd = activity.getContentResolver().openFileDescriptor(path, "w"); assert pfd != null;
                        FileOutputStream fileOutputStream = new FileOutputStream(pfd.getFileDescriptor());
                        ArrayList<String> total = new ArrayList<>(columns.get(0).size());
                        switch (requestCode) {
                            case Constants.Requests.SAVE_CSV_ALL:
                                total.add("TA ("+columns.size()+")"); for(int i=1; i<columns.get(0).size(); i++) total.add("0");
                                for(int i=0;i<columns.get(0).size();i++) {
                                    for(int j=0;j<columns.size();j++) {
                                        fileOutputStream.write((columns.get(j).get(i)+",").getBytes());
                                        if(i>0) { if(columns.get(j).get(i).equals("P")) total.set(i, String.valueOf(Integer.parseInt(total.get(i))+1)); }
                                    }
                                    fileOutputStream.write((total.get(i)+"\n").getBytes());
                                }
                                break;
                            case Constants.Requests.SAVE_CSV_MONTH:
                                int k = monthList.getSelectedItemPosition(); total.add("TA ("+(positions.get(k+1)-positions.get(k))+")");
                                for(int i=1; i<columns.get(0).size(); i++) total.add("0");
                                for(int i=0;i<columns.get(0).size();i++) {
                                    for(int j=0;j<2;j++) {
                                        fileOutputStream.write((columns.get(j).get(i)+",").getBytes());
                                    }
                                    for(int j=positions.get(k);j<positions.get(k+1);j++) {
                                        fileOutputStream.write((columns.get(j).get(i)+",").getBytes());
                                        if(i>0) { if(columns.get(j).get(i).equals("P")) total.set(i, String.valueOf(Integer.parseInt(total.get(i))+1)); }
                                    }
                                    fileOutputStream.write((total.get(i)+"\n").getBytes());
                                }
                                break;
                        }
                        fileOutputStream.close(); pfd.close();
                        Snackbar.make(root, "Attendance saved", BaseTransientBottomBar.LENGTH_SHORT).show();
                    }
                    catch (Exception e) {
                        Log.e(TAG, "error: ",e);
                        Snackbar.make(root, "Attendance could not be saved. Try again.", BaseTransientBottomBar.LENGTH_SHORT).show();
                    }
                }
            }
        }
        else super.onActivityResult(requestCode, resultCode, data);
    }
}
