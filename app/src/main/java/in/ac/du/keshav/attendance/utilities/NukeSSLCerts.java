package in.ac.du.keshav.attendance.utilities;

// This class helps avoid verification of SSL certificates.
// https://newfivefour.com/android-trust-all-ssl-certificates.html

import android.annotation.SuppressLint;
import android.util.Log;

import java.security.SecureRandom;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.cert.X509Certificate;

public class NukeSSLCerts {
    private static final String TAG = NukeSSLCerts.class.getSimpleName();
    public void nuke() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    @SuppressLint("TrustAllX509TrustManager")
                    @Override public void checkClientTrusted(X509Certificate[] chain, String authType) { }
                    @SuppressLint("TrustAllX509TrustManager")
                    @Override public void checkServerTrusted(X509Certificate[] chain, String authType) { }
                    public X509Certificate[] getAcceptedIssuers() { return new X509Certificate[0]; }
                }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @SuppressLint("BadHostnameVerifier") @Override
                public boolean verify(String arg0, SSLSession arg1) { return true; }
            });
        } catch (Exception e) {
          Log.e(TAG, "error: ", e);
        }
    }
}
