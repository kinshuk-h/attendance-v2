package in.ac.du.keshav.attendance.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.regex.Pattern;

import in.ac.du.keshav.attendance.R;
import in.ac.du.keshav.attendance.data.Paper;
import in.ac.du.keshav.attendance.data.Student;
import in.ac.du.keshav.attendance.data.Teacher;
import in.ac.du.keshav.attendance.data.User;
import in.ac.du.keshav.attendance.utilities.CoreUtils;
import in.ac.du.keshav.attendance.utilities.Constants;

public class LoginActivity extends AppCompatActivity {

    private static String TAG = LoginActivity.class.getName();

    private String user_id, user_pw;
    private ProgressDialog dialog;
    private int count = 0;

    public User loadUser() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String name        = prefs.getString(Constants.Data.NAME        , null),
               accessToken = prefs.getString(Constants.Data.ACCESS_TOKEN, null),
               type        = prefs.getString(Constants.Data.USER_TYPE   , null),
               userId      = prefs.getString(Constants.Data.USER_ID     , null),
               papers      = prefs.getString(Constants.Data.PAPERS      , null);
        if(!CoreUtils.allSet(name, accessToken, type, userId, papers)) return null;
        User user = null; assert(type!=null);
        if(type.equals("Student")) {
            Student s = new Student(); s.setAccessToken(accessToken); s.name = name; s.rollNo = userId;
            s.course = prefs.getString(Constants.Data.COURSE, null);
            s.group = prefs.getString(Constants.Data.GROUP, null);
            try {
                JSONArray array = new JSONArray(prefs.getString(Constants.Data.PAPERS, null));
                for(int i=0;i<array.length();i++) {
                    Paper paper = new Paper(array.getJSONObject(i));
                    s.papers.add(paper);
                    Log.i(TAG, "Paper added: "+paper.toJSON());
                }
            } catch (JSONException e) { Log.e(LoginActivity.class.getName(),"error: "+e); e.printStackTrace(); }
            user = s;
        }
        else if(type.equals("Teacher")) {
            Teacher t = new Teacher(); t.setAccessToken(accessToken); t.name = name; t.email = userId;
            try {
                JSONArray array = new JSONArray(prefs.getString(Constants.Data.PAPERS, null));
                for(int i=0;i<array.length();i++) {
                    Paper paper = new Paper(array.getJSONObject(i)); t.papers.add(paper);
                }
            } catch (JSONException e) { Log.e(LoginActivity.class.getName(),"error: "+e); e.printStackTrace(); }
            user = t;
        }
        return user;
    }
    public void saveUser(User user) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
        editor.putString(Constants.Data.NAME, user.name);
        editor.putString(Constants.Data.USER_ID, user.getUserId());
        editor.putString(Constants.Data.USER_TYPE, user.getType());
        editor.putString(Constants.Data.ACCESS_TOKEN, user.getAccessToken());
        try {
            JSONArray papers = new JSONArray();
            for(Paper paper : user.papers) { papers.put(paper.toJSON()); }
            Log.e(TAG, "Array: "+papers);
            editor.putString(Constants.Data.PAPERS, papers.toString());
        } catch (JSONException e) { Log.e(TAG,"error: "+e); e.printStackTrace(); }
        if(user instanceof Teacher) {
            Teacher t = (Teacher) user;
            editor.putString(Constants.Data.USER_PASSWORD, t.password);
            editor.putString(Constants.Data.COURSE, null);
            editor.putString(Constants.Data.GROUP, null);
        }
        else if(user instanceof Student) {
            Student s = (Student) user;
            editor.putString(Constants.Data.USER_PASSWORD, null);
            editor.putString(Constants.Data.COURSE, s.course);
            editor.putString(Constants.Data.GROUP, s.group);
        }
        editor.apply();
    }

    private JsonObjectRequest getRequest() {
        HashMap<String, String> data = new HashMap<>();
        data.put(Constants.Data.USER_ID, user_id);
        data.put(Constants.Data.USER_PASSWORD, user_pw);
        data.put(Constants.Data.MAC_ADDRESS, CoreUtils.getMACAddress());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                Constants.URL.Users+"?"+CoreUtils.toQueryString(data), null,
                response -> {
                    try {
                        if(dialog!=null) dialog.setMessage("Fetching user credentials ... ");
                        User user = null;
                        if(response.getString(Constants.Data.USER_TYPE).equals("Student")) {
                            Student s = new Student();
                            s.name = response.getString(Constants.Data.NAME);
                            s.rollNo = response.getString(Constants.Data.USER_ID);
                            s.course = response.getString(Constants.Data.COURSE);
                            s.group = response.getString(Constants.Data.GROUP);
                            JSONArray papers = response.getJSONArray(Constants.Data.PAPERS);
                            for(int i=0;i<papers.length();i++) {
                                Paper paper = new Paper(papers.getJSONObject(i));
                                s.papers.add(paper);
                            }
                            s.setAccessToken(response.getString(Constants.Data.ACCESS_TOKEN));
                            user = s;
                        }
                        else if(response.getString(Constants.Data.USER_TYPE).equals("Teacher")) {
                            Teacher t = new Teacher();
                            t.name = response.getString(Constants.Data.NAME);
                            t.email = response.getString(Constants.Data.USER_ID);
                            t.setAccessToken(response.getString(Constants.Data.ACCESS_TOKEN));
                            JSONArray papers = response.getJSONArray(Constants.Data.PAPERS);
                            for(int i=0;i<papers.length();i++) {
                                Paper paper = new Paper(papers.getJSONObject(i));
                                t.papers.add(paper);
                            }
                            user = t;
                        }
                        if(user!=null) {
                            saveUser(user); if(dialog!=null) dialog.dismiss();
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.putExtra(Constants.Data.USER, user); finish(); startActivity(intent);
                        }
                        else Toast.makeText(getApplicationContext(), "The server didn't respond. Try again.", Toast.LENGTH_SHORT).show();
                    }
                    catch (JSONException e) {
                        Toast.makeText(getApplicationContext(), R.string.login_error_default, Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "error: ", e); dialog.dismiss();
                    }
                },
                e -> {
                    Log.e(TAG, "error: ", e);
                    if(e instanceof TimeoutError) {
                        count++; if(count==Constants.RETRY_ATTEMPT_COUNT) {
                            Toast.makeText(getApplicationContext(), R.string.login_error_timeout, Toast.LENGTH_SHORT).show(); finish();
                        }
                        Toast.makeText(getApplicationContext(), "Request timed out. Retrying...", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        if(e.networkResponse!=null) {
                            Log.e(TAG, new String(e.networkResponse.data, StandardCharsets.UTF_8));
                            dialog.dismiss(); switch(e.networkResponse.statusCode) {
                                case 401: Toast.makeText(getApplicationContext(), R.string.login_error_401, Toast.LENGTH_SHORT).show(); break;
                                case 403: Toast.makeText(getApplicationContext(), R.string.login_error_403, Toast.LENGTH_SHORT).show(); break;
                                default: Toast.makeText(getApplicationContext(), R.string.login_error_default, Toast.LENGTH_SHORT).show(); break;
                            }
                        }
                    }
                }
        ){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                Log.e(TAG, "Status Code: " + response.statusCode);
                Log.e(TAG, "Response   : \n" + CoreUtils.toString(response.data));
                NetworkResponse new_response = new NetworkResponse(response.statusCode, "{}".getBytes(), true, response.networkTimeMs, response.allHeaders);
                return super.parseNetworkResponse(response.statusCode != 200 ? new_response : response);
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000,
                Constants.RETRY_ATTEMPT_COUNT, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        return request;
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        User user = loadUser(); if(user!=null) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.putExtra(Constants.Data.USER, user); finish(); startActivity(intent);
        }

        super.onCreate(savedInstanceState); setContentView(R.layout.activity_login);

        final EditText email_roll = findViewById(R.id.email_roll), password = findViewById(R.id.password);
        final TextInputLayout password_layout = findViewById(R.id.password_layout);

        final RequestQueue queue = Volley.newRequestQueue(this);

        email_roll.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
                email_roll.setError(null);
                if(Pattern.matches(Constants.Regex.EMAIL_REGEX, s)) {
                    if(password_layout.getVisibility()==View.GONE) password_layout.setVisibility(View.VISIBLE);
                } else {
                    if(password_layout.getVisibility()==View.VISIBLE) password_layout.setVisibility(View.GONE);
                }
            }
            @Override public void afterTextChanged(Editable s) { }
        });

        MaterialButton sign_in = findViewById(R.id.btn_sign_in);
        sign_in.setOnClickListener(v -> {
            if(!Pattern.matches(Constants.Regex.EMAIL_REGEX, email_roll.getText().toString()) &&
               !Pattern.matches(Constants.Regex.ROLL_NO_REGEX, email_roll.getText().toString())) {
                email_roll.setError("Invalid Roll Number / Email"); return;
            }
            if(password_layout.getVisibility()==View.VISIBLE && password.getText().toString().isEmpty()) {
                password.setError("Password cannot be empty"); return;
            }
            user_id = email_roll.getText().toString(); user_pw = password.getText().toString();
            dialog = new ProgressDialog(LoginActivity.this);
            dialog.setMessage("Trying to sign in ...");
            dialog.show(); queue.add(getRequest());
        });
    }
}
