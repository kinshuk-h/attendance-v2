package in.ac.du.keshav.attendance;

import android.app.Application;

import in.ac.du.keshav.attendance.utilities.NukeSSLCerts;

public class Attendance extends Application {
    @Override public void onCreate() {
        super.onCreate();
        new NukeSSLCerts().nuke(); // Disable SSL verification.
    }
}
