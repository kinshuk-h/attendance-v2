package in.ac.du.keshav.attendance.services;

import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.HashMap;
import java.util.Map;

import in.ac.du.keshav.attendance.utilities.Constants;
import in.ac.du.keshav.attendance.utilities.CoreUtils;

public class FCMService extends FirebaseMessagingService {
    @Override public void onNewToken(@NonNull final String token) {
        StringRequest request = new StringRequest(com.android.volley.Request.Method.POST, Constants.URL.Users,
                new Response.Listener<String>() {
                    @Override public void onResponse(String response) { }
                },
                new Response.ErrorListener() {
                    @Override public void onErrorResponse(VolleyError error) {
                        Log.e(FCMService.class.getName(), "error: ", error);
                    }
                })
        {
            @Override protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put(Constants.Data.MAC_ADDRESS, CoreUtils.getMACAddress());
                params.put(Constants.Data.DEVICE_TOKEN, token);
                return params;
            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(request);
        Intent intent = new Intent(Constants.Actions.NEW_TOKEN);
        intent.putExtra(Constants.Data.DEVICE_TOKEN, token);
        sendBroadcast(intent);
    }
    @Override public void onMessageReceived(@NonNull RemoteMessage message) {
        //super.onMessageReceived(message);
        Log.i(FCMService.class.getName(), "New message!");
        Log.i(FCMService.class.getName(), message.getData().toString());
        Intent intent = new Intent(Constants.Actions.NEW_MESSAGE);
        intent.putExtra(Constants.Data.MAP, new HashMap<>(message.getData()));
        intent.putExtra(Constants.Data.SOURCE, message.getFrom());
        sendBroadcast(intent);
    }
    @Override public void onDeletedMessages() { super.onDeletedMessages(); }
}
