package in.ac.du.keshav.attendance.data;

import java.util.HashMap;
import java.util.Map;

import in.ac.du.keshav.attendance.utilities.Constants;

public class Request
{
    public enum State { APPROVED, DECLINED, PENDING }

    private State state = State.PENDING;
    private String rollNo, creatorRollNo, name;
    private Paper paper = new Paper();

    public String getRollNo () { return rollNo;        }
    public String getCreator() { return creatorRollNo; }
    public State  getState  () { return state;         }
    public String getName   () { return name;          }
    public Paper  getPaper  () { return paper;         }

    private Request() { }
    public Request(String creatorRollNo, String rollNo) {
        this.creatorRollNo = creatorRollNo; this.rollNo = rollNo;
    }

    public void setName  (String name) { this.name = name;   }
    public void setState (State state) { this.state = state; }
    public void setPaper (Paper paper) { this.paper = paper; }

    public boolean equals(Request other) {
        if(other==null) return false;
        return creatorRollNo.equals(other.creatorRollNo) &&
               rollNo.equals(other.rollNo) && paper.equals(other.paper);
    }
    public static Request from(Map<String, String> map) {
        Request request = new Request();
        request.creatorRollNo = map.get(Constants.Data.CREATOR);
        request.rollNo = map.get(Constants.Data.ROLL_NO);
        if(request.paper!=null) {
            request.paper.paperId = map.get(Constants.Data.PAPER_ID);
            request.paper.group = map.get(Constants.Data.GROUP);
            request.paper.name = map.get(Constants.Data.PAPER);
            if(map.containsKey(Constants.Data.PAPER_TYPE)) {
                String type = map.get(Constants.Data.PAPER_TYPE);
                if(type == null || type.isEmpty()) type = "Theory";
                switch (type) {
                    case "Theory": request.paper.type = Paper.Type.Theory; break;
                    case "Practical": request.paper.type = Paper.Type.Practical; break;
                    case "Tutorial": request.paper.type = Paper.Type.Tutorial; break;
                }
            }
            else request.paper.type = Paper.Type.Theory;
        }
        if(map.containsKey(Constants.Data.REQUEST_NAME))
            request.name = map.get(Constants.Data.REQUEST_NAME);
        else request.name = "";
        if(map.containsKey(Constants.Data.HANDLED) && map.containsKey(Constants.Data.APPROVED)) {
            String approved = map.get(Constants.Data.APPROVED), handled = map.get(Constants.Data.HANDLED);
            if(handled !=null && !handled.isEmpty() && (handled.equals("1") || handled.equalsIgnoreCase("TRUE"))) {
                if(approved!=null && (approved.equals("1") || approved.equalsIgnoreCase("TRUE")))
                    request.state = State.APPROVED;
                else request.state = State.DECLINED;
            } else request.state = State.PENDING;
        } else request.state = State.PENDING;
        return request;
    }
    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<>();
        map.put(Constants.Data.ROLL_NO, rollNo);
        map.put(Constants.Data.CREATOR, creatorRollNo);
        if(paper!=null) {
            map.put(Constants.Data.PAPER_ID, paper.paperId);
            map.put(Constants.Data.PAPER, paper.name);
            map.put(Constants.Data.GROUP, paper.group);
            map.put(Constants.Data.PAPER_TYPE, paper.type.name());
        }
        if(name!=null) map.put(Constants.Data.REQUEST_NAME, name);
        map.put(Constants.Data.HANDLED, state!=State.PENDING ? "1":"0");
        map.put(Constants.Data.APPROVED, state==State.APPROVED ? "1":"0");
        return map;
    }
}
