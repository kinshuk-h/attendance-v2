package in.ac.du.keshav.attendance.data;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import in.ac.du.keshav.attendance.utilities.Constants;

public class Paper implements Serializable {
    String name, paperId, group; Type type;
    public enum Type { Theory, Practical, Tutorial }

    Paper() {}
    public Paper(JSONObject object) throws JSONException {
        group = object.getString(Constants.Data.GROUP);
        paperId = object.getString(Constants.Data.PAPER_ID);
        name = object.getString(Constants.Data.PAPER);
        if(object.has(Constants.Data.PAPER_TYPE)) {
            switch (object.getString(Constants.Data.PAPER_TYPE)) {
                case "Theory": type = Type.Theory; break;
                case "Practical": type = Type.Practical; break;
                case "Tutorial": type = Type.Tutorial; break;
            }
        }
        else type = Type.Theory;
    }
    public JSONObject toJSON() throws JSONException {
        JSONObject object = new JSONObject();
        object.put(Constants.Data.PAPER, name);
        object.put(Constants.Data.PAPER_ID, paperId);
        object.put(Constants.Data.GROUP, group);
        object.put(Constants.Data.PAPER_TYPE, type.name());
        return object;
    }
    public String getPaperId() { return paperId;     }
    public String getType   () { return type.name(); }
    public String getGroup  () { return group;       }
    public String getName   () { return name;        }
    public int getTypeIndex() {
        int pos = 0; for(Type type : Type.values()) {
            if(type.equals(this.type)) return pos; pos++; } return -1;
    }
    public String getNameAndType () { return getName()+" - "+getType();                       }
    public String getCompleteName() { return getName()+" - "+getType()+" - Sec. "+getGroup(); }
    public String getCodeName    () { return getName()+" - "+getPaperId();                    }
    public boolean equals(Paper paper) {
        return paper.group.equals(group) && paper.paperId.equals(paperId) && paper.type.equals(type);
    }
}