package in.ac.du.keshav.attendance.data;

public class Student extends User {
    public String rollNo, course, group;
    public String getUserId() { return rollNo; }
    public String getType() { return "Student"; }
}
