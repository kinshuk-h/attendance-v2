package in.ac.du.keshav.attendance.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreference;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import in.ac.du.keshav.attendance.R;
import in.ac.du.keshav.attendance.activities.MainActivity;
import in.ac.du.keshav.attendance.data.Paper;
import in.ac.du.keshav.attendance.data.Student;
import in.ac.du.keshav.attendance.data.Teacher;
import in.ac.du.keshav.attendance.utilities.Constants;

public class AccountFragment extends Fragment {

    public static class SettingsFragment extends PreferenceFragmentCompat {
        private MainActivity activity;
        private SwitchPreference notifications;
        @Override public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            activity = (MainActivity)getActivity();
            setPreferencesFromResource(R.xml.root_preferences, rootKey);
            Preference logout = findPreference(Constants.Preferences.LOGOUT);
            notifications = findPreference(Constants.Preferences.NOTIFICATIONS);
            if(logout!=null) {
                logout.setOnPreferenceClickListener(preference -> {
                    if(activity!=null) activity.logout(false); return true;
                });
            }
            if(notifications!=null) {
                notifications.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        final int state = notifications.isChecked()?0:1; notifications.setEnabled(false);
                        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL.Users,
                                response -> {
                                    Toast.makeText(activity.getApplicationContext(), "Preferences changed successfully.", Toast.LENGTH_SHORT).show();
                                    notifications.setEnabled(true);
                                },
                                error -> {
                                    Log.d(AccountFragment.class.getName(), "error: ",error);
                                    notifications.setEnabled(true); notifications.setChecked(state==0);
                                    if(error.networkResponse!=null) {
                                        Log.e(AccountFragment.class.getName(), "Response: "+new String(error.networkResponse.data, StandardCharsets.UTF_8));
                                        Toast.makeText(activity.getApplicationContext(), R.string.login_error_default, Toast.LENGTH_SHORT).show();
                                    }
                                })
                        {
                            @Override public Map<String, String> getHeaders() throws AuthFailureError {
                                return activity.getUser() == null ? super.getHeaders() : activity.getHeaders();
                            }
                            @Override public Map<String, String> getParams() {
                                Map<String, String> map = new HashMap<>();
                                map.put("notifications",String.valueOf(state));
                                return map;
                            }
                        };
                        if(activity != null) activity.getQueue().add(request);
                        return true;
                    }
                });
            }
        }
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstance) {
        return inflater.inflate(R.layout.fragment_account, parent, false);
    }
    @Override public void onViewCreated(@NonNull View view, Bundle savedInstance) {
        super.onViewCreated(view, savedInstance);
        getChildFragmentManager().beginTransaction().replace(R.id.sub_fragment_view, new SettingsFragment()).commit();
    }
    @Override public void onActivityCreated(Bundle savedInstance) {
        super.onActivityCreated(savedInstance);
        MainActivity activity = (MainActivity) getActivity();
        View root = getView();
        if (root != null && activity != null) {
            TextView name = root.findViewById(R.id.name), type = root.findViewById(R.id.type),
                     attr1 = root.findViewById(R.id.attr1), attr2 = root.findViewById(R.id.attr2),
                     val1 = root.findViewById(R.id.val1), val2 = root.findViewById(R.id.val2);
            name.setText(activity.getUser().name);
            if(activity.getUser() instanceof Student) {
                Student user = (Student) activity.getUser();
                type.setText(R.string.student);
                attr1.setText(R.string.roll_number); attr2.setText(R.string.course);
                val1.setText(user.rollNo); val2.setText((user.course+" - Sec. "+user.group));
            } else {
                Teacher user = (Teacher) activity.getUser();
                type.setText(R.string.teacher);
                attr1.setText(R.string.email); attr2.setText(R.string.papers);
                StringBuilder papers = new StringBuilder();
                for(Paper paper : user.papers) {
                    papers.append(paper.getCompleteName()).append('\n');
                }
                val1.setText(user.email); val2.setText(papers.substring(0, papers.length()-1));
            }
        }
    }
}
