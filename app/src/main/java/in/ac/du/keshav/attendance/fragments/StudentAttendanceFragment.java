package in.ac.du.keshav.attendance.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.ac.du.keshav.attendance.R;
import in.ac.du.keshav.attendance.UI.TableAdapter;
import in.ac.du.keshav.attendance.activities.MainActivity;
import in.ac.du.keshav.attendance.data.Paper;
import in.ac.du.keshav.attendance.data.PaperMetrics;
import in.ac.du.keshav.attendance.data.Student;
import in.ac.du.keshav.attendance.utilities.Constants;
import in.ac.du.keshav.attendance.utilities.CoreUtils;

public class StudentAttendanceFragment extends Fragment {

    private static final String TAG = StudentAttendanceFragment.class.getName();

    private ProgressBar progress;
    private TextView noRecords;
    private RecyclerView recyclerView;

    private MainActivity activity;

    private void loadAttendance(final MainActivity.ActionCompleteHandler handler) {
        progress.setVisibility(View.VISIBLE); noRecords.setVisibility(View.GONE); recyclerView.setVisibility(View.GONE);

        final Student user = (Student) activity.getUser();

        final JsonObjectRequest request = new JsonObjectRequest(
                Constants.URL.Userdata+"?"+Constants.Data.ROLL_NO+"="+user.rollNo, null,
                response -> {
                    progress.setVisibility(View.GONE); recyclerView.setVisibility(View.VISIBLE);
                    TableAdapter adapter = new TableAdapter(activity); recyclerView.setAdapter(adapter);
                    String paperCode = ""; PaperMetrics metrics = null; int index = -1;
                    try {
                        JSONArray papers = response.getJSONArray(Constants.Data.PAPERS);
                        JSONArray dates = response.getJSONArray(Constants.Data.DATES);

                        for(int i=0;i<papers.length();i++) {
                            JSONObject paper = papers.getJSONObject(i);

                            if(paperCode.isEmpty() || !paperCode.equals(paper.getString(Constants.Data.PAPER_ID))) {
                                paperCode = paper.getString(Constants.Data.PAPER_ID);
                                if(metrics != null) adapter.add(metrics); metrics = new PaperMetrics();
                            }

                            for(Paper p : user.papers) {
                                if(p.getPaperId().equals(paperCode) &&
                                   p.getType().equals(paper.getString(Constants.Data.PAPER_TYPE)))
                                { index = p.getTypeIndex(); metrics.name = p.getCodeName(); break; }
                            }
                            for(int j=0;j<dates.length();j++) {
                                String month = CoreUtils.getMonth(dates.getString(j));
                                if(!metrics.records.containsKey(month)) metrics.add(month);
                                String mark = paper.getString(dates.getString(j));
                                ArrayList<PaperMetrics.Count> count = metrics.records.get(month); assert count != null;
                                if(index != -1) {
                                    if(!mark.equals("-")) count.get(index).total++;
                                    if(mark.equals("P")) count.get(index).current++;
                                }
                            }
                        }
                        if(metrics != null) adapter.add(metrics);
                        if(adapter.getItemCount()==0) {
                            recyclerView.setVisibility(View.GONE); noRecords.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        recyclerView.setVisibility(View.GONE); noRecords.setVisibility(View.VISIBLE);
                        Log.e(TAG, "error: ", e);
                    }
                    if(handler != null) handler.onActionComplete();
                },
                e -> {
                    Log.e(TAG, "error: ", e);
                    progress.setVisibility(View.GONE); noRecords.setVisibility(View.VISIBLE);
                    if(e.networkResponse!=null) {
                        Log.e(TAG, "Status: "+e.networkResponse.statusCode);
                        Log.e(TAG, "Response: "+CoreUtils.toString(e.networkResponse.data));
                    }
                    if(handler != null) handler.onActionComplete();
                })
        {
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                Log.e(TAG, "Status Code: " + response.statusCode);
                Log.e(TAG, "Response   : \n" + CoreUtils.toString(response.data));
                NetworkResponse new_response = new NetworkResponse(response.statusCode, "{}".getBytes(), true, response.networkTimeMs, response.allHeaders);
                return super.parseNetworkResponse(response.statusCode != 200 ? new_response : response);
            }
        };
        activity.getQueue().add(request);

    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstance) {
        return inflater.inflate(R.layout.fragment_attendance_student, parent, false);
    }
    @Override public void onActivityCreated(Bundle savedInstance) {
        super.onActivityCreated(savedInstance);
        activity = (MainActivity) getActivity(); View root = getView();
        final SwipeRefreshLayout layout = (SwipeRefreshLayout) root;

        if(root!=null) {
            progress = root.findViewById(R.id.progress);
            noRecords = root.findViewById(R.id.no_records);
            recyclerView = root.findViewById(R.id.recycler_view);
        }
        if(activity!=null && layout!=null) {
            layout.setOnRefreshListener(() -> {
                layout.setRefreshing(true);
                MainActivity.ActionCompleteHandler handler = () -> layout.setRefreshing(false);
                loadAttendance(handler);
            });

            loadAttendance(null);
        }
    }
}
