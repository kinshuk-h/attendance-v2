package in.ac.du.keshav.attendance.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.ac.du.keshav.attendance.R;
import in.ac.du.keshav.attendance.UI.RequestAdapter;
import in.ac.du.keshav.attendance.data.MainViewModel;
import in.ac.du.keshav.attendance.data.Paper;
import in.ac.du.keshav.attendance.data.Request;
import in.ac.du.keshav.attendance.data.Student;
import in.ac.du.keshav.attendance.data.Teacher;
import in.ac.du.keshav.attendance.data.User;
import in.ac.du.keshav.attendance.fragments.AccountFragment;
import in.ac.du.keshav.attendance.fragments.HomeFragment;
import in.ac.du.keshav.attendance.fragments.StudentAttendanceFragment;
import in.ac.du.keshav.attendance.fragments.TeacherAttendanceFragment;
import in.ac.du.keshav.attendance.utilities.CoreUtils;
import in.ac.du.keshav.attendance.utilities.Constants;

public class MainActivity extends AppCompatActivity {

    private static String TAG = MainActivity.class.getName();

    private Fragment homeFragment = new HomeFragment(), attendanceFragment,
                     accountFragment = new AccountFragment();
    private TextView header; private BottomNavigationView nav_bar;
    private Menu menu;

    public interface ActionCompleteHandler { void onActionComplete(); }

    void displayLegend() {
        if(model.currentUser != null) {
            if(model.currentUser instanceof Student) Log.e(TAG, "Display Student Legend.");
            else Log.d(TAG, "Display Teacher Legend");
        }
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu); this.menu = menu; return true;
    }
    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                if (model.currentUser != null) {
                    if (model.currentUser instanceof Student) loadPendingRequests();
                    else if (model.currentUser.currentPaper != null)
                        loadRequests(model.currentUser.currentPaper);
                }
                break;
            case R.id.help: displayLegend(); break;
            default: return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private MainViewModel model; private StringRequest request;
    private BroadcastReceiver token_receiver, message_receiver;

    public Map<String, String> getHeaders() {
        Map<String,String> params = new HashMap<>();
        String credentials = model.currentUser.getUserId()+":"+model.currentUser.getAccessToken();
        String hash = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        params.put(Constants.Headers.CONTENT_TYPE,"application/x-www-form-urlencoded");
        params.put(Constants.Headers.AUTHORIZATION, "Basic "+hash);
        return params;
    }
    public Map<String, String> getJSONHeaders() {
        Map<String,String> params = new HashMap<>();
        String credentials = model.currentUser.getUserId()+":"+model.currentUser.getAccessToken();
        String hash = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        params.put(Constants.Headers.CONTENT_TYPE,"application/json");
        params.put(Constants.Headers.AUTHORIZATION, "Basic "+hash);
        return params;
    }

    public RequestAdapter getAdapter() {
        if(model.adapter == null) {
            model.adapter = new RequestAdapter(this, model.currentUser instanceof Student);
            model.adapter.addRequestStatusListener(new RequestAdapter.RequestStatusListener() {
                @Override public void onRequestStatusChange(final Request request) {
                    Log.e(TAG, "Paper: "+request.getPaper().getCodeName());
                    if(model.adapter != null) {
                        int pos = model.adapter.indexOf(request);
                        if(pos<model.adapter.getItemCount()) model.adapter.erase(pos);
                    }
                    StringRequest new_request = new StringRequest(com.android.volley.Request.Method.POST, Constants.URL.Requests,
                            response -> Log.e(TAG, response),
                            error -> {
                                if(model.adapter != null) {
                                    request.setState(Request.State.PENDING); model.adapter.add(request);
                                }
                                Log.e(TAG, "error: ", error);
                                if(error.networkResponse != null) {
                                    Log.e(TAG, "Status code: "+error.networkResponse.statusCode);
                                    Log.e(TAG, "Response: "+ CoreUtils.toString(error.networkResponse.data));
                                }
                                Toast.makeText(getApplicationContext(), R.string.login_error_default, Toast.LENGTH_SHORT).show();
                            })
                    {
                        @Override public Map<String, String> getHeaders() throws AuthFailureError {
                            return model.currentUser == null ? super.getHeaders() : MainActivity.this.getHeaders();
                        }
                        @Override public Map<String, String> getParams() { return request.toMap(); }
                    };
                    model.queue.add(new_request);
                }
            });
        }
        return model.adapter;
    }

    public RequestQueue getQueue() { return model.queue; }
    public User getUser() { return model.currentUser; }
    public void clearLoginCredentials() {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
        editor.putString(Constants.Data.USER_ID, null);
        editor.putString(Constants.Data.USER_PASSWORD, null);
        editor.putString(Constants.Data.NAME, null);
        editor.putString(Constants.Data.USER_TYPE, null);
        editor.putString(Constants.Data.PAPERS, null);
        editor.putString(Constants.Data.COURSE, null);
        model.currentUser = null;
        editor.apply(); startActivity(new Intent(MainActivity.this, LoginActivity.class)); finish();
    }
    public void clearLoginCredentials(SharedPreferences.Editor editor) {
        editor.putString(Constants.Data.USER_ID, null);
        editor.putString(Constants.Data.USER_PASSWORD, null);
        editor.putString(Constants.Data.NAME, null);
        editor.putString(Constants.Data.USER_TYPE, null);
        editor.putString(Constants.Data.PAPERS, null);
        editor.putString(Constants.Data.COURSE, null);
    }
    public void login() {
        final Response.ErrorListener errorListener = error -> {
            Log.e(TAG, "error: ", error);
            findViewById(R.id.no_connection_layout).setVisibility(View.VISIBLE);
            findViewById(R.id.fragment_view).setVisibility(View.INVISIBLE);
            TextView issue = findViewById(R.id.issue_message);
            if(error.networkResponse!=null) {
                issue.setText(R.string.server_error);
                Log.e(TAG, "Error Code "+error.networkResponse.statusCode);
                Log.e(TAG, "Response: "+CoreUtils.toString(error.networkResponse.data));
                clearLoginCredentials();
            }
            else issue.setText(R.string.no_connection);
            findViewById(R.id.retry).setOnClickListener(v -> model.queue.add(request));
        };
        request = new StringRequest(com.android.volley.Request.Method.POST, Constants.URL.Users,
            new Response.Listener<String>() {
                @Override public void onResponse(String response) {
                    model.queue.cancelAll(request -> true);
                    request = new StringRequest(com.android.volley.Request.Method.GET,
                            Constants.URL.Users + "?login",
                            response1 -> {
                                nav_bar.setSelectedItemId(R.id.home);
                                findViewById(R.id.no_connection_layout).setVisibility(View.GONE);
                                findViewById(R.id.fragment_view).setVisibility(View.VISIBLE);
                                model.queue.cancelAll(request -> true);
                            }, errorListener)
                    {
                        @Override public Map<String, String> getHeaders() throws AuthFailureError {
                            return model.currentUser == null ? super.getHeaders() : MainActivity.this.getHeaders();
                        }
                    };
                    model.queue.add(request);
                }
            }, errorListener)
        {
            @Override protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put(Constants.Data.MAC_ADDRESS, CoreUtils.getMACAddress());
                params.put(Constants.Data.DEVICE_TOKEN, model.device_token);
                return params;
            }
        };
        model.queue.add(request);
    }
    public void logout(final boolean isRoutine) {
        StringRequest request = new StringRequest(com.android.volley.Request.Method.GET,
                Constants.URL.Users + "?logout"+(!isRoutine?"&clear":""),
                response -> {
                    model.orientationChange = false;
                    if(!isRoutine) { clearLoginCredentials(); }
                },
                error -> {
                    Log.e(TAG, "error: ", error);
                    Toast.makeText(getApplicationContext(), R.string.login_error_timeout, Toast.LENGTH_SHORT).show();
                })
        {
            @Override public Map<String, String> getHeaders() throws AuthFailureError {
                return model.currentUser == null ? super.getHeaders() : MainActivity.this.getHeaders();
            }
        };
        model.queue.add(request);
    }

    @SuppressWarnings("unchecked")
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); setContentView(R.layout.activity_main);

        model = new ViewModelProvider(this).get(MainViewModel.class);
        model.currentUser = (User) getIntent().getSerializableExtra(Constants.Data.USER);
        if(model.currentUser==null) { finish(); startActivity(new Intent(this, LoginActivity.class)); }
        if(model.queue==null) model.queue = Volley.newRequestQueue(getApplicationContext());

        attendanceFragment = model.currentUser instanceof Student ? new StudentAttendanceFragment() : new TeacherAttendanceFragment();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if(prefs.getInt(Constants.Preferences.VERSION, 0) != Constants.Preferences.DEFAULT_VERSION) {
            Toast.makeText(getApplicationContext(), "Session Expired. Try again.", Toast.LENGTH_SHORT).show();
            SharedPreferences.Editor editor = prefs.edit(); clearLoginCredentials(editor);
            editor.putInt(Constants.Preferences.VERSION, Constants.Preferences.DEFAULT_VERSION); editor.apply();
            logout(false); startActivity(new Intent(this, LoginActivity.class)); finish();
        }

        Toolbar toolbar = findViewById(R.id.main_toolbar); setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.main_menu); header = findViewById(R.id.main_header);

        token_receiver = new BroadcastReceiver() {
            @Override public void onReceive(Context context, Intent intent) {
                saveNewToken(intent.getStringExtra(Constants.Data.DEVICE_TOKEN));
            }
        };
        registerReceiver(token_receiver, new IntentFilter(Constants.Actions.NEW_TOKEN));
        message_receiver = new BroadcastReceiver() {
            @Override public void onReceive(Context context, Intent intent) {
                handleNewMessage((HashMap<String,String>)intent.getSerializableExtra(Constants.Data.MAP));
            }
        };
        registerReceiver(message_receiver, new IntentFilter(Constants.Actions.NEW_MESSAGE));

        nav_bar = findViewById(R.id.bottom_nav_bar);
        nav_bar.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.home: loadFragment(model.headers.get(0), homeFragment); break;
                case R.id.attendance: loadFragment(model.headers.get(1), attendanceFragment); break;
                case R.id.account: loadFragment(model.headers.get(2), accountFragment); break;
            }
            return true;
        });
        if(savedInstanceState==null) {
            FirebaseInstanceId.getInstance().getInstanceId()
            .addOnCompleteListener(task -> {
                if(!task.isSuccessful()) { Log.e(TAG, "error: "+task.getException()); finish(); return; }
                if(task.getResult()!=null) { saveNewToken(task.getResult().getToken()); login(); }
            });
        }
    }
    @Override public void onStart() {
        super.onStart(); model.orientationChange = false;
    }
    @Override public void onDestroy() {
        if(!model.orientationChange) {
            if(model.currentUser!=null) { logout(true); }
        }
        unregisterReceiver(token_receiver);
        unregisterReceiver(message_receiver);
        super.onDestroy();
    }

    @Override public void onSaveInstanceState(@NonNull Bundle savedInstance) {
        super.onSaveInstanceState(savedInstance); model.orientationChange = true;
    }

    @Override public void onBackPressed() {
        super.onBackPressed(); int lastIndex = getSupportFragmentManager().getBackStackEntryCount()-1;
        if(getSupportFragmentManager().getBackStackEntryCount()==0) { finish(); return; }
        FragmentManager.BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(lastIndex);
        header.setText(entry.getName()); nav_bar.getMenu().getItem(model.headers.indexOf(entry.getName())).setChecked(true);
    }

    public void loadPendingRequests(final ActionCompleteHandler handler) {
        if(model.adapter != null) {
            model.adapter.clear();
            JsonArrayRequest data_request = new JsonArrayRequest(com.android.volley.Request.Method.GET,
                    Constants.URL.Requests, null,
                    response -> {
                        try {
                            for(int i=0;i<response.length();i++) {
                                JSONObject object = (JSONObject) response.get(i);
                                Request request = new Request(object.getString(Constants.Data.CREATOR),
                                        object.getString(Constants.Data.ROLL_NO));
                                if(model.currentUser instanceof Teacher)
                                    request.setName(object.getString(Constants.Data.REQUEST_NAME));
                                request.setPaper(new Paper(object));
                                String approved = object.getString(Constants.Data.APPROVED);
                                String handled = object.getString(Constants.Data.HANDLED);
                                if(handled.isEmpty() || handled.equals("0")) {
                                    request.setState(Request.State.PENDING);
                                } else {
                                    if(!approved.isEmpty() && approved.equals("1")) request.setState(Request.State.APPROVED);
                                    else request.setState(Request.State.DECLINED);
                                }
                                model.adapter.add(request);
                            }
                            if(handler != null) handler.onActionComplete();
                        } catch (JSONException e) { Log.e(TAG, "error: ", e); }
                        if(handler != null) handler.onActionComplete();
                    },
                    error -> {
                        Log.e(TAG, "error: ", error);
                        if(error.networkResponse!=null) {
                            Log.e(TAG, "Status Code: "+error.networkResponse.statusCode);
                            Log.e(TAG, "Response   : \n"+ CoreUtils.toString(error.networkResponse.data));
                        }
                        if(handler != null) handler.onActionComplete();
                    })
            {
                @Override public Map<String, String> getHeaders() throws AuthFailureError {
                    return model.currentUser!=null ? MainActivity.this.getHeaders() : super.getHeaders();
                }
            };
            getQueue().add(data_request);
        }
    }
    public void loadPendingRequests() { loadPendingRequests(null); }
    public void loadRequests(@NonNull final Paper paper, final ActionCompleteHandler handler) {
        if(model.adapter != null) {
            model.adapter.clear(); HashMap<String, String> map = new HashMap<>();
            map.put(Constants.Data.PAPER_ID, paper.getPaperId());
            map.put(Constants.Data.GROUP, paper.getGroup());
            map.put(Constants.Data.PAPER_TYPE, paper.getType());
            JsonArrayRequest data_request = new JsonArrayRequest(com.android.volley.Request.Method.GET,
                    Constants.URL.Requests + "?" + CoreUtils.toQueryString(map), null,
                    response -> {
                        try {
                            for(int i=0;i<response.length();i++) {
                                JSONObject object = (JSONObject) response.get(i);
                                Request request = new Request(object.getString(Constants.Data.CREATOR),
                                        object.getString(Constants.Data.ROLL_NO));
                                if(model.currentUser instanceof Teacher)
                                    request.setName(object.getString(Constants.Data.REQUEST_NAME));
                                request.setPaper(paper);
                                String approved = object.getString(Constants.Data.APPROVED);
                                String handled = object.getString(Constants.Data.HANDLED);
                                if(handled.isEmpty() || handled.equals("0")) {
                                    request.setState(Request.State.PENDING);
                                } else {
                                    if(!approved.isEmpty() && approved.equals("1")) request.setState(Request.State.APPROVED);
                                    else request.setState(Request.State.DECLINED);
                                }
                                model.adapter.add(request);
                            }
                        } catch (JSONException e) { Log.e(TAG, "error: ", e); }
                        if(handler != null) handler.onActionComplete();
                    },
                    error -> {
                        Log.e(TAG, "error: ", error);
                        if(error.networkResponse!=null) {
                            Log.e(TAG, "Status Code: "+error.networkResponse.statusCode);
                            Log.e(TAG, "Response   : \n"+ CoreUtils.toString(error.networkResponse.data));
                        }
                        if(handler != null) handler.onActionComplete();
                    })
            {
                @Override public Map<String, String> getHeaders() throws AuthFailureError {
                    return model.currentUser!=null ? MainActivity.this.getHeaders() : super.getHeaders();
                }
                @Override public Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    Log.e(TAG, "Status Code: " + response.statusCode);
                    Log.e(TAG, "Response   : \n" + CoreUtils.toString(response.data));
                    NetworkResponse new_response = new NetworkResponse(response.statusCode, "[]".getBytes(), true, response.networkTimeMs, response.allHeaders);
                    return super.parseNetworkResponse(response.statusCode != 200 ? new_response : response);
                }
            };
            getQueue().add(data_request);
        }
    }
    public void loadRequests(@NonNull Paper paper) { loadRequests(paper, null); }
    public void fetchRequests(final Paper paper) {
        StringRequest request = new StringRequest(com.android.volley.Request.Method.POST, Constants.URL.Requests,
                response -> {
                    Log.i(TAG, "Response: "+response); loadRequests(paper); },
                error -> {
                    Log.e(TAG, "error: ", error);
                    if(error.networkResponse != null) {
                        Log.e(TAG, "Status code: "+error.networkResponse.statusCode);
                        Log.e(TAG, "Response: "+CoreUtils.toString(error.networkResponse.data));
                    }
                    Toast.makeText(getApplicationContext(), R.string.login_error_default, Toast.LENGTH_SHORT).show();
                })
        {
            @Override public Map<String, String> getHeaders() throws AuthFailureError {
                return model.currentUser != null ? MainActivity.this.getHeaders() : super.getHeaders();
            }
            @Override public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(Constants.Data.PAPER_ID, paper.getPaperId());
                params.put(Constants.Data.PAPER, paper.getName());
                params.put(Constants.Data.GROUP, paper.getGroup());
                params.put(Constants.Data.PAPER_TYPE, paper.getType());
                return params;
            }
        };
        model.queue.add(request);
    }
    public void sendRequest(final Request request) {
        StringRequest new_request = new StringRequest(com.android.volley.Request.Method.POST, Constants.URL.Requests,
                response -> Log.e(TAG, response),
                error -> {
                    Log.e(TAG, "error: ", error);
                    if(error.networkResponse != null) {
                        Log.e(TAG, "Status code: "+error.networkResponse.statusCode);
                        Log.e(TAG, "Response: "+CoreUtils.toString(error.networkResponse.data));
                        Toast.makeText(getApplicationContext(), R.string.request_error_default, Toast.LENGTH_SHORT).show();
                    } else Toast.makeText(getApplicationContext(), R.string.login_error_default, Toast.LENGTH_SHORT).show();
                    if(!(error instanceof TimeoutError)) {
                        if(model.adapter != null) {
                            int pos = model.adapter.indexOf(request);
                            if(pos<model.adapter.getItemCount()) model.adapter.erase(pos);
                        }
                    }
                })
        {
            @Override public Map<String, String> getHeaders() throws AuthFailureError {
                return model.currentUser != null ? MainActivity.this.getHeaders() : super.getHeaders();
            }
            @Override public Map<String, String> getParams() { return request.toMap(); }
        };
        model.queue.add(new_request);
    }
    public void sendAllRequestsWith(Request.State state) {
        if(model.adapter!=null && model.adapter.getItemCount()!=0) {
            JSONArray array = new JSONArray(); final ArrayList<Request> requests = new ArrayList<>(model.adapter.items());
            for(Request req : requests) { req.setState(state); array.put(new JSONObject(req.toMap())); } model.adapter.clear();
            JsonArrayRequest request = new JsonArrayRequest(com.android.volley.Request.Method.POST, Constants.URL.Requests, array,
                    response -> { },
                    error -> {
                        if(!(error instanceof TimeoutError)) {
                            for(Request r : requests) { r.setState(Request.State.PENDING); model.adapter.add(r); }
                        }
                        Log.e(TAG, "error: ", error);
                        if(error.networkResponse != null) {
                            Log.e(TAG, "Status code: "+error.networkResponse.statusCode);
                            Log.e(TAG, "Response: "+CoreUtils.toString(error.networkResponse.data));
                        }
                    })
            {
                @Override public Map<String, String> getHeaders() throws AuthFailureError {
                    return model.currentUser == null ? super.getHeaders() : getJSONHeaders();
                }
                @Override public Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    Log.e(TAG, "Status code: "+response.statusCode);
                    Log.e(TAG, "Response: "+CoreUtils.toString(response.data));
                    NetworkResponse new_response = new NetworkResponse(response.statusCode, "[]".getBytes(), true, response.networkTimeMs, response.allHeaders);
                    return super.parseNetworkResponse(response.statusCode == 200 ? new_response : response);
                }
            };
            model.queue.add(request);
        }
    }
    public void saveNewToken(String token) {
        if(token==null) { return; } model.device_token = token;
        Log.d(TAG, "New Token: "+model.device_token.substring(0,20)+"...");
    }
    public void handleNewMessage(HashMap<String, String> data) {
        //StringBuilder str = new StringBuilder();
        //Log.d(TAG, "Source: "+source);
        //Log.d(TAG, "Data: ");
        //for(Map.Entry<String, String> key : data.entrySet()) {
        //    Log.d(TAG, key.getKey()+": "+key.getValue());
        //    str.append(key.getKey()).append(": ").append(key.getValue()).append("\n");
        //}
        //new AlertDialog.Builder(this).setTitle(source).setMessage(str.toString()).setCancelable(true).create().show();
        if(model.adapter!=null) {
            if(model.currentUser instanceof Student) model.adapter.insertOrAssign(Request.from(data));
            else {
                Request new_request = Request.from(data);
                if(model.currentUser.currentPaper != null && model.currentUser.currentPaper.equals(new_request.getPaper()))
                    model.adapter.insertOrAssign(new_request);
            }
        }
    }

    void loadFragment(String title, Fragment fragment) {
        header.setText(title);
        if(menu != null) {
            if(title.equals(model.headers.get(0))) {
                menu.setGroupVisible(R.id.dashboard_group, true);
                menu.setGroupVisible(R.id.attendance_group, false);
            }
            else if(title.equals(model.headers.get(1))) {
                menu.setGroupVisible(R.id.dashboard_group, false);
                //menu.setGroupVisible(R.id.attendance_group, true);
            }
            else {
                menu.setGroupVisible(R.id.dashboard_group, false);
                menu.setGroupVisible(R.id.attendance_group, false);
            }
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_view, fragment).addToBackStack(title).commitAllowingStateLoss();
    }
}
