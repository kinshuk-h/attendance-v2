package in.ac.du.keshav.attendance.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PaperMetrics {
    public static class Count {
        public int current = 0, total = 0;
    }
    public String name;
    public HashMap<String, ArrayList<Count>> records = new HashMap<>();
    public ArrayList<Count> getCumulative() {
        ArrayList<Count> cumulative_count = new ArrayList<>();
        for(int i=0;i<3;i++) cumulative_count.add(new Count());
        for(Map.Entry<String, ArrayList<Count>> record : records.entrySet()) {
            for(int i=0; i<record.getValue().size(); i++) {
                cumulative_count.get(i).current += record.getValue().get(i).current;
                cumulative_count.get(i).total += record.getValue().get(i).total;
            }
        }
        return cumulative_count;
    }
    public void add(String month) {
        ArrayList<Count> count = new ArrayList<>();
        for(int i=0;i<3;i++) count.add(new Count());
        records.put(month, count);
    }
}
