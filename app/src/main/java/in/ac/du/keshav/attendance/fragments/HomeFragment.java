package in.ac.du.keshav.attendance.fragments;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import in.ac.du.keshav.attendance.R;
import in.ac.du.keshav.attendance.UI.RequestAdapter;
import in.ac.du.keshav.attendance.activities.MainActivity;
import in.ac.du.keshav.attendance.data.Paper;
import in.ac.du.keshav.attendance.data.Request;
import in.ac.du.keshav.attendance.data.Student;
import in.ac.du.keshav.attendance.data.Teacher;

public class HomeFragment extends Fragment {

    private MainActivity activity;
    private ArrayAdapter<String> paper_adapter;
    private EditText input;
    private Spinner paper;
    private AlertDialog dialog;
    private RequestAdapter adapter;

    @Override public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup parent, Bundle savedInstance) {
        return inflater.inflate(R.layout.fragment_home, parent, false);
    }
    @Override public void onActivityCreated(Bundle savedInstance) {
        super.onActivityCreated(savedInstance);
        activity = (MainActivity) getActivity(); View root = getView();
        if(root!=null && activity!=null) {
            final TextView count = root.findViewById(R.id.request_count);
            final RelativeLayout request_layout = root.findViewById(R.id.requests_section);
            final LinearLayout pill_box = root.findViewById(R.id.pill_box),
                               empty_layout = root.findViewById(R.id.no_request);
            final FloatingActionButton add = root.findViewById(R.id.btn_add);
            final MaterialButton accept_all = root.findViewById(R.id.btn_accept_all),
                                 deny_all = root.findViewById(R.id.btn_deny_all),
                                 fetch = root.findViewById(R.id.btn_fetch_new);
            final SwipeRefreshLayout layout = (SwipeRefreshLayout) root;
            layout.setOnRefreshListener(() -> {
                layout.setRefreshing(true);
                MainActivity.ActionCompleteHandler handler = () -> layout.setRefreshing(false);
                if(activity.getUser() instanceof Student) activity.loadPendingRequests(handler);
                else activity.loadRequests(activity.getUser().currentPaper, handler);
            });

            adapter = activity.getAdapter();

            RequestAdapter.OnRequestListener requestListener = new RequestAdapter.OnRequestListener() {
                @Override public void onNoRequests() {
                    empty_layout.setVisibility(View.VISIBLE);
                    request_layout.setVisibility(View.GONE);
                    if(pill_box.getVisibility()==View.VISIBLE) pill_box.setVisibility(View.GONE);
                    count.setText(R.string.request_count_default);
                }
                @Override public void onNewRequests(boolean afterNoRequests) {
                    if(afterNoRequests) {
                        empty_layout.setVisibility(View.GONE);
                        request_layout.setVisibility(View.VISIBLE);
                        if(activity.getUser() instanceof Teacher && pill_box.getVisibility()==View.GONE)
                            pill_box.setVisibility(View.VISIBLE);
                    }
                    count.setText((adapter.getItemCount()+" "+(adapter.getItemCount()==1?"request":"requests")));
                }
            };
            if(adapter.getItemCount()>0) requestListener.onNewRequests(true); else requestListener.onNoRequests();
            adapter.addOnRequestListener(requestListener);

            RecyclerView requests_list = root.findViewById(R.id.request_list); requests_list.setAdapter(adapter);

            if(activity.getUser() instanceof Teacher) {
                add.setVisibility(View.GONE); fetch.setVisibility(View.VISIBLE);
                accept_all.setOnClickListener(v -> {
                    if(adapter!=null && adapter.getItemCount()>0) {
                        DialogInterface.OnClickListener dialogListener = (dialog, which) -> {
                            if(which == DialogInterface.BUTTON_POSITIVE) activity.sendAllRequestsWith(Request.State.APPROVED);
                        };
                        AlertDialog dialog = new AlertDialog.Builder(activity)
                                .setTitle(R.string.action_title).setMessage("Are you sure that you would like to accept all requests?")
                                .setPositiveButton("YES", dialogListener)
                                .setNegativeButton("NO", dialogListener)
                                .setCancelable(true).create();
                        dialog.show();
                    }
                    else Toast.makeText(activity.getApplicationContext(), R.string.no_requests, Toast.LENGTH_SHORT).show();
                });
                deny_all.setOnClickListener(v -> {
                    if(adapter!=null && adapter.getItemCount()>0) {
                        DialogInterface.OnClickListener dialogListener = (dialog, which) -> {
                            if(which == DialogInterface.BUTTON_POSITIVE) activity.sendAllRequestsWith(Request.State.DECLINED);
                        };
                        AlertDialog dialog = new AlertDialog.Builder(activity)
                                .setTitle(R.string.action_title).setMessage("Are you sure that you would like to deny all requests?")
                                .setPositiveButton("YES", dialogListener)
                                .setNegativeButton("NO", dialogListener)
                                .setCancelable(true).create();
                        dialog.show();
                    }
                    else Toast.makeText(activity.getApplicationContext(), R.string.no_requests, Toast.LENGTH_SHORT).show();
                });
                fetch.setOnClickListener(v -> {
                    if(activity.getUser().currentPaper != null)
                        activity.fetchRequests(activity.getUser().currentPaper);
                });

                if(paper_adapter == null) {
                    ArrayList<String> list = new ArrayList<>();
                    for(Paper p : activity.getUser().papers) list.add(p.getCompleteName());
                    paper_adapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, list);
                    paper_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                }
                paper = root.findViewById(R.id.teacher_paper); paper.setAdapter(paper_adapter); paper.setVisibility(View.VISIBLE);
                paper.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        activity.getUser().currentPaper = activity.getUser().papers.get(position);
                        if(activity.getUser().currentPaper != null) activity.loadRequests(activity.getUser().currentPaper);
                    }
                    @Override public void onNothingSelected(AdapterView<?> parent) { }
                });
            }
            else {
                pill_box.setVisibility(View.GONE); add.setVisibility(View.VISIBLE);

                NestedScrollView scrollView = root.findViewById(R.id.scroll_view);
                scrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                    int dy = scrollY - oldScrollY;
                    if (dy > 0 && add.getVisibility() == View.VISIBLE) { add.hide(); }
                    else if (dy < 0 && add.getVisibility() != View.VISIBLE) { add.show(); }
                });

                final Student student = (Student) activity.getUser();
                if(dialog == null) {
                    @SuppressLint("InflateParams")
                    View dialog_view = LayoutInflater.from(activity).inflate(R.layout.dialog_request, null, false);
                    input = dialog_view.findViewById(R.id.input); paper = dialog_view.findViewById(R.id.paper);
                    if(paper_adapter == null) {
                        ArrayList<String> list = new ArrayList<>();
                        for(Paper p : student.papers) list.add(p.getNameAndType());
                        paper_adapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, list);
                        paper_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    }
                    paper.setAdapter(paper_adapter); input.setText(student.rollNo);
                    dialog = new AlertDialog.Builder(activity)
                            .setPositiveButton("ADD", (dialog, which) -> { })
                            .setView(dialog_view).setCancelable(true).setTitle("Create Request").create();
                }
                add.setOnClickListener(v -> {
                    dialog.show(); dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(v1 -> {
                        if(input.getText().toString().isEmpty()) input.setError("Field cannot be empty");
                        else {
                            Request request = new Request(student.rollNo, input.getText().toString());
                            int position = paper_adapter.getPosition(paper.getSelectedItem().toString());
                            request.setPaper(student.papers.get(position));
                            if(activity.getAdapter().add(request)) { activity.sendRequest(request); dialog.dismiss(); }
                            else { input.setError("Request already sent"); }
                        }
                    });
                });
                activity.loadPendingRequests();
            }
        }
    }
}
