package in.ac.du.keshav.attendance.data;

import androidx.lifecycle.ViewModel;

import com.android.volley.RequestQueue;

import java.util.Arrays;
import java.util.List;

import in.ac.du.keshav.attendance.UI.RequestAdapter;

public class MainViewModel extends ViewModel {
    public User currentUser;

    public RequestAdapter adapter;
    public RequestQueue queue;

    public boolean orientationChange = false;
    public String device_token;

    public List<String> headers = Arrays.asList("Dashboard", "Attendance", "My Account");
}
