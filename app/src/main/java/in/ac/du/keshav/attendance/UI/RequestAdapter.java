package in.ac.du.keshav.attendance.UI;

import android.content.Context;
import android.graphics.Canvas;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.ac.du.keshav.attendance.R;
import in.ac.du.keshav.attendance.data.Request;
import in.ac.du.keshav.attendance.utilities.CoreUtils;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ViewHolder> {
    private ArrayList<Request> requests = new ArrayList<>();
    private Context context;
    private boolean view_mode;
    private RequestStatusListener listener;
    private OnRequestListener requestListener;

    public interface RequestStatusListener {
        void onRequestStatusChange(Request request);
    }
    public interface OnRequestListener {
        void onNoRequests(); void onNewRequests(boolean afterNoRequests);
    }

    class SwipeCallback extends ItemTouchHelper.SimpleCallback {
        SwipeCallback() { super(0, ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT); }
        @Override public boolean onMove(@NonNull RecyclerView recyclerView,
                                        @NonNull RecyclerView.ViewHolder viewHolder,
                                        @NonNull RecyclerView.ViewHolder target) { return false; }

        @Override public void onSwiped(@NonNull RecyclerView.ViewHolder holder, int direction) {
            switch (direction) {
                case ItemTouchHelper.RIGHT:
                    requests.get(holder.getAdapterPosition()).setState(Request.State.APPROVED);
                    listener.onRequestStatusChange(requests.get(holder.getAdapterPosition()));
                    break;
                case ItemTouchHelper.LEFT:
                    requests.get(holder.getAdapterPosition()).setState(Request.State.DECLINED);
                    listener.onRequestStatusChange(requests.get(holder.getAdapterPosition()));
                    break;
            }
        }
        @Override public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
            if (viewHolder != null) {
                ViewHolder holder = (ViewHolder) viewHolder;
                getDefaultUIUtil().onSelected(holder.foreground);
                holder.foreground.setSelected(true);
            }
        }
        @Override public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView,
                                          @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                          int actionState, boolean isCurrentlyActive) {
            if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                ViewHolder holder = (ViewHolder) viewHolder;
                if(dX < 0) { holder.bg_decline.setVisibility(View.VISIBLE); holder.bg_accept.setVisibility(View.GONE); }
                if(dX > 0) { holder.bg_accept.setVisibility(View.VISIBLE); holder.bg_decline.setVisibility(View.GONE); }
                getDefaultUIUtil().onDraw(c, recyclerView, holder.foreground,
                        dX, dY, actionState, isCurrentlyActive);
            }
        }
        @Override public void clearView(@NonNull RecyclerView recyclerView,
                              @NonNull RecyclerView.ViewHolder viewHolder) {
            ViewHolder holder = (ViewHolder) viewHolder;
            getDefaultUIUtil().clearView(holder.foreground);
            holder.foreground.setSelected(false);
        }
        @Override public float getSwipeThreshold(@NonNull RecyclerView.ViewHolder viewHolder) { return 0.7f; }
    }

    public RequestAdapter(Context context, boolean inViewMode) {
        this.context = context; view_mode = inViewMode;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        View root, bg_accept, bg_decline, foreground, divider; TextView roll_no, state, name, paper;
        ViewHolder(View root) {
            super(root); this.root = root; foreground = root.findViewById(R.id.foreground);
            bg_accept = root.findViewById(R.id.bg_accept); bg_decline = root.findViewById(R.id.bg_decline);
            name = foreground.findViewById(R.id.name); roll_no = foreground.findViewById(R.id.roll_number);
            state = foreground.findViewById(R.id.state); paper = foreground.findViewById(R.id.paper);
            divider = root.findViewById(R.id.divider);
        }
    }

    @Override public void onAttachedToRecyclerView(@NonNull RecyclerView view) {
        super.onAttachedToRecyclerView(view);
        if(!view_mode) new ItemTouchHelper(new SwipeCallback()).attachToRecyclerView(view);
    }

    @NonNull @Override public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_request, parent, false));
    }
    @Override public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.roll_no.setText(requests.get(position).getRollNo());
        if(position == 0) holder.divider.setVisibility(View.INVISIBLE);
        else holder.divider.setVisibility(View.VISIBLE);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams
                (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        if(view_mode) {
            holder.name.setVisibility(View.GONE); holder.state.setVisibility(View.VISIBLE);
            holder.state.setText(requests.get(position).getState().name());
            holder.paper.setText(requests.get(position).getPaper().getNameAndType());
            params.addRule(RelativeLayout.ALIGN_PARENT_START);
        }
        else {
            holder.state.setVisibility(View.GONE); holder.paper.setVisibility(View.GONE);
            if(requests.get(position).getName() == null ||
                    requests.get(position).getName().isEmpty()) holder.name.setVisibility(View.GONE);
            else { holder.name.setVisibility(View.VISIBLE); holder.name.setText(requests.get(position).getName()); }
            if(!requests.get(position).getCreator().equals(requests.get(position).getRollNo()))
                holder.name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_proxy,0,0,0);
            else holder.name.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
            params.addRule(RelativeLayout.ALIGN_PARENT_END);
        }
        holder.roll_no.setLayoutParams(params);
    }

    @Override public int getItemCount() { return requests.size(); }
    public void clear() {
        requests.clear(); notifyDataSetChanged();
        if(requestListener!=null) requestListener.onNoRequests();
    }
    public ArrayList<Request> items() { return requests; }
    //public Request getItem(int position) { return requests.get(position); }
    public void insertOrAssign(Request request) {
        int pos = indexOf(request); if(pos!=-1) assign(pos, request); else add(request);
    }
    private void assign(int position, Request request) {
        requests.set(position, request); notifyItemChanged(position);
    }
    public boolean add(Request request) {
        if(indexOf(request)!=-1) return false;
        requests.add(request); notifyItemInserted(requests.size()-1);
        if(!requests.isEmpty() && requestListener!=null) {
            if(requests.size() == 1) requestListener.onNewRequests(true);
            else requestListener.onNewRequests(false);
        }
        return true;
    }
    public void erase(int position) {
        requests.remove(position); notifyItemRemoved(position);
        if(requestListener != null) {
            if(requests.isEmpty()) requestListener.onNoRequests();
            else requestListener.onNewRequests(false);
        }
    }
    public int indexOf(Request request) {
        for(int i=0;i<requests.size();i++)
            if(requests.get(i).equals(request)) return i;
        return -1;
    }

    public void addRequestStatusListener(RequestStatusListener listener) { this.listener = listener; }
    public void addOnRequestListener(OnRequestListener requestListener) { this.requestListener = requestListener; }
}
